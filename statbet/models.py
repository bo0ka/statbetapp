import copy
import pandas as pd

from django.conf import settings
from django.db.models import Max, Sum
from django.db import models
from django.db.models import ObjectDoesNotExist
from django.core.exceptions import MultipleObjectsReturned


class Country(models.Model):
    name = models.CharField(max_length=30, unique=True, default='', verbose_name='Страна')

    def __str__(self):
        return self.name


class Stage(models.Model):
    name = models.CharField(max_length=120, unique=True, default='', verbose_name='Состояние')

    def __str__(self):
        return self.name


class Team(models.Model):
    class Meta:
        unique_together = ('name', 'country')
    name = models.CharField(max_length=100, default='', verbose_name='Команда')
    country = models.ForeignKey(Country, null=True, verbose_name='Страна')

    def __str__(self):
        return self.name


class CupGroup(models.Model):
    class Meta:
        unique_together = ('name', 'country')
    name = models.CharField(max_length=100, default='', verbose_name='Серия')
    country = models.ForeignKey(Country, null=True, verbose_name='Страна')

    def __str__(self):
        return '%s: %s' % (self.country.name, self.name)


class Cup(models.Model):
    class Meta:
        unique_together = ('name', 'country')
    name = models.CharField(max_length=100, default='', verbose_name='Чемпионат')
    country = models.ForeignKey(Country, null=True, verbose_name='Страна')
    season_start = models.DateField(null=True, verbose_name='Начало сезона')
    season = models.CharField(max_length=30, default='', verbose_name='Сезон')
    group = models.ForeignKey(CupGroup, null=True, verbose_name='Серия')

    def __str__(self):
        return '%s: %s' % (self.country.name, self.name)

    def get_prev_cup(self):
        prev_cup_started = self.season_start.replace(year=self.season_start.year - 1)
        try:
            prev_cup = Cup.objects.get(group=self.group, season_start=prev_cup_started)
        except ObjectDoesNotExist:
            return None
        except MultipleObjectsReturned:
            prev_cup = Cup.objects.filter(
                group=self.group,
                season_start=prev_cup_started
            ).order_by('-season_start').first()
        return prev_cup


class Sport(models.Model):
    name = models.CharField(max_length=20, unique=True, default='Soccer', verbose_name='Спорт')

    def __str__(self):
        return self.name


class Event(models.Model):
    WIN_FIRST = 'W1'
    WIN_SECOND = 'W2'
    WIN_DRAW = 'WX'
    WIN_NONE = 'WN'
    WIN_NEW = '--'
    WINNING_STATES = (
        (WIN_FIRST, 'Победа'),
        (WIN_SECOND, 'Поражение'),
        (WIN_DRAW, 'Ничья'),
        (WIN_NONE, 'Не учитывать'),
        (WIN_NEW, 'Новое'),
    )
    event_id = models.CharField(max_length=8, unique=True, default='', db_index=True, verbose_name='ID события',
                                editable=False)
    sport = models.ForeignKey(Sport, null=True, verbose_name='Спорт')
    country = models.ForeignKey(Country, null=True, verbose_name='Страна')
    cup = models.ForeignKey(Cup, null=True, verbose_name='Чемпоинат')
    date = models.DateField(null=True, verbose_name='Дата события')
    stage = models.ForeignKey(Stage, null=True, verbose_name='Состояние окончания')
    team1 = models.ForeignKey(Team, related_name='team1', null=True, verbose_name='Дома')
    team2 = models.ForeignKey(Team, related_name='team2', null=True, verbose_name='Гости')
    score = models.CharField(max_length=20, default='', verbose_name='Счет')
    paritial_score = models.CharField(max_length=30, default='', verbose_name='Детально')
    url = models.URLField(default='', verbose_name='Источник')
    # winnig_state = models.CharField(max_length=2, choices=WINNING_STATES, default=WIN_NEW)
    # th - home team wins count, ta - away team wins count at event date
    # TODO: devide rest to other model
    th = models.PositiveIntegerField(default=0, verbose_name='Сыграно дома')
    ta = models.PositiveIntegerField(default=0, verbose_name='Сыграно в гостях')
    ratio = models.DecimalField(verbose_name='Отношение сил', max_digits=6, decimal_places=1, default=0)
    ratio_bid = models.DecimalField(verbose_name='Отношение ставок', max_digits=6, decimal_places=1, default=0)

    def __str__(self):
        return self.event_id

    def get_current_group(self, position=None, now=None):
        """
        Получить группу для команды, если она играла в прошлом чемпионате
        :param
            position:       'home' или 'away', также допускаю 1 или 2
        :return:
            int от 1 до 4 или 0 в случае отсутствия
        """
        if now is None:
            now = False
        if not (isinstance(position, (str, int)) and position in ['away', 'home', 1, 2]):
            return 0
        home = False
        required_team = self.team2
        if position in ['home', 1]:
            required_team = self.team1
            home = True
        if not now:
            required_cup = self.cup.get_prev_cup()
        else:
            required_cup = self.cup

        try:
            ct = CupTeam.objects.get(cup=required_cup, team=required_team)
        except CupTeam.DoesNotExist:
            return 0

        if home:
            return ct.cupteamgroup.group_home
        return ct.cupteamgroup.group_away

    def team_formula(self, team, pts_param):
        """
        Анализ силы команды
        :param pts_param: имя поля модели MatchResult которое использовать при суммировании очей
        :param team: команда

        :return: Коэффициент силы
        Формула 1
        Начало сезона не анализируем, пропускаем первые 5 матчей, т.е. начинаем с 5 тура.
        Берем команды и по каждой из них делаем анализ их 5 предыдущих матчей :
        Анализируем каждый матч по формуле :
        Команда 1 = Матч 1 (результат (кол-во очков 3/1/0) * {кф силы команды}) + Матч 2 (результат (кол-во очков 3/1/0) * {кф силы команды}) + Матч 3 (результат (кол-во очков 3/1/0) * {кф силы команды}) + Матч 4 (результат (кол-во очков 3/1/0) * {кф силы команды}) +Матч 5 (результат (кол-во очков 3/1/0) * {кф силы команды})
        Команда 2 = Матч 1 (результат (кол-во очков 3/1/0) * {кф силы команды}) + Матч 2 (результат (кол-во очков 3/1/0) * {кф силы команды}) + Матч 3 (результат (кол-во очков 3/1/0) * {кф силы команды}) + Матч 4 (результат (кол-во очков 3/1/0) * {кф силы команды}) +Матч 5 (результат (кол-во очков 3/1/0) * {кф силы команды})
        Матч = команда 1 / команда 2
        {кф силы команды} - в зависимости от кол-ва очков в чемпионате :
        1 место = 1 (100%)
        2 место = (очки команды на 1 месте)/(очки команды на 2 месте)
        3 место = (очки команды на 1 месте)/(очки команды на 3 месте) ...

        При подсчете по формулам может получиться отрицательное число или равное 0.
        В этом случае можно кф. команды приводить к 1, а сумму которую нужно было добавить для такого приведения добавляем к другой команде.
        """
        cup = self.cup
        teams = CupTeam.objects.filter(cup=cup).values_list('team', flat=True)
        matches = MatchResult.objects.filter(event__cup=cup, team=team, event__date__lt=self.date).order_by('-event__date')
        if matches:
            mc = matches.count()
            matches = matches[:mc >= settings.BET_PTS_MAX_GAMES and settings.BET_PTS_MAX_GAMES or mc]

        result = 0.0
        # Турнирная таблица
        # TODO: убедиться что getattr(r, pts_param) а не r.pts
        tt = sorted(
            [
                (t, sum([getattr(r, pts_param) for r in MatchResult.objects.filter(
                    team__id=t, event__cup=cup, event__date__lt=self.date
                )])) for t in teams
                ], key=lambda x: x[1], reverse=True)
        dict_tt = dict(tt)
        # Очки лидера
        lead_pts = tt[0][1]
        for match in matches:
            if match.event.team1.id != team.id:
                match_team_id = match.event.team1.id
            else:
                match_team_id = match.event.team2.id
            if lead_pts and dict_tt[match_team_id]:
                result += getattr(match, pts_param) * dict_tt[match_team_id]/lead_pts
            else:
                result += getattr(match, pts_param) * (dict_tt[match_team_id] + 1) / (lead_pts + 1)
        return result

    def team_formula1(self, team):
        return self.team_formula(team, 'pts')

    def team_formula2(self, team):
        return self.team_formula(team, 'pts_formula2')

    def get_bid(self, market_param, avg=None, bookmaker=None, is_max=None):
        """
        Получить значение коэффициента букмекера, средний или максимальный 
        :param market_param: Объект параметра рынков 
        :param avg: Среднее значение параметра рынка
        :param bookmaker: Получить по объеекту букмекера
        :param is_max: Получить максимальный среди имеющихся
        :return: Значение
        """
        if avg is None and bookmaker is None:
            avg = True
        else:
            avg = False
        if is_max is None:
            is_max = False
        if is_max:
            avg = False
        filter_dict = dict(market_parameter=market_param, is_average=avg)
        if not bookmaker is None:
            filter_dict['bookmaker'] = bookmaker
        bids = self.bet_set.filter(**filter_dict)
        if is_max:
            return bids.aggregate(Max('bid'))['bid__max']
        if bids:
            return bids[0].bid
        return 0

    def get_bid_uni(self, **kwargs):
        # TODO: must be renamed n be wo uni
        market_param = None
        avg = 'avg' in kwargs and kwargs['avg'] or None
        bookmaker = 'bookmaker' in kwargs and kwargs['bookmaker'] or None
        is_max = 'is_max' in kwargs and kwargs['is_max'] or None
        if 'market_param' in kwargs:
            market_param = kwargs['market_param']
        return self.get_bid(market_param, avg=avg, bookmaker=bookmaker, is_max=is_max)

    def get_forecast(self, depth, matches_use=None, team_groups=(), default=None, type=None):
        """
        Получить расчет по прогнозу события
        :param depth: глубина расчета
        :param matches_use: минимальное кол-во матчей
        :param team_groups: группы команд, <tuple>
        :param default: значение по-умолчанию при неуспешном расчете
        :param type: фильтр типа анализа, OptimalForecast.WIN_CHOICES
        :return: 1, 2 или default
        """
        def chk(x):
            if not (x['matches__sum'] and x['matches__sum'] >= matches_use):
                return False
            return True

        type_values = []
        if type and type in dict(OptimalForecast.WIN_CHOICES):
            if type == OptimalForecast.WIN_SET:
                type_values = [int(OptimalForecast.WIN_HOME), int(OptimalForecast.WIN_AWAY), ]
            else:
                type_values = [int(type), ]
        forecast_value = default
        if not matches_use:
            matches_use = settings.BET_ACCEPT_GAMES
        if not team_groups:
            team_groups = (self.get_current_group(1), self.get_current_group(2))
            
        ratio = self.ratio
        l_ctotal = lambda cup_group, t_g, where, ratio_of: CupTotal.objects.filter(
            cup__group=cup_group,
            cup__season_start__lt=self.cup.season_start,
            group=t_g,
            where=where,
            ratio_of=ratio_of,
            ratio=ratio,
        ).order_by('-cup__season_start')[:depth].aggregate(Sum('value'), Sum('matches'))

        home_ratio = l_ctotal(self.cup.group, team_groups[0], CupTotal.HOME, CupTotal.RATIO)
        home_ratio_bid = l_ctotal(self.cup.group, team_groups[0], CupTotal.HOME, CupTotal.R12)
        away_ratio = l_ctotal(self.cup.group, team_groups[1], CupTotal.AWAY, CupTotal.RATIO)
        away_ratio_bid = l_ctotal(self.cup.group, team_groups[1], CupTotal.AWAY, CupTotal.R12)

        if not all([chk(away_ratio_bid), chk(away_ratio), chk(home_ratio), chk(home_ratio_bid)]):
            return default

        home_pwr = round(home_ratio['value__sum'] / home_ratio['matches__sum'])
        home_bid = round(home_ratio_bid['value__sum'] / home_ratio_bid['matches__sum'])
        away_pwr = round(away_ratio['value__sum'] / away_ratio['matches__sum'])
        away_bid = round(away_ratio_bid['value__sum'] / away_ratio_bid['matches__sum'])
        
        if home_pwr > away_pwr and home_bid > away_bid:
            forecast_value = 1
        if home_pwr < away_pwr and home_bid < away_bid:
            forecast_value = 2
        if type_values and forecast_value not in type_values:
            forecast_value = default
        return forecast_value

    def get_win(self):
        splited_score = self.score.split(':')
        if len(splited_score) == 2:
            if splited_score[0] > splited_score[1]:
                return 1
            elif splited_score[1] > splited_score[0]:
                return 2
            else:
                return 0
        return '-'


class Market(models.Model):
    name = models.CharField(max_length=30, default='', db_index=True, verbose_name='Рынок')
    sysname = models.CharField(max_length=3, default='', db_index=True, unique=True)

    def __str__(self):
        return self.name


class Bookmaker(models.Model):

    class Meta:
        unique_together = ('name', 'bookmaker_id', )
    name = models.CharField(max_length=50, default='', unique=True, db_index=True, verbose_name='Букмейкер')
    url = models.URLField(verbose_name='URL')
    bookmaker_id = models.IntegerField(verbose_name='BetExplorer Bookmaker ID', db_index=True, default=0)

    def __str__(self):
        return self.name


class MarketParameter(models.Model):
    name = models.CharField(max_length=20, default='', verbose_name='Параметр')
    market = models.ForeignKey(Market, null=True, verbose_name='Рынок')
    is_odd = models.BooleanField(default=True, verbose_name='Ставка?')
    order = models.PositiveIntegerField(default=0, verbose_name='Порядок')

    def __str__(self):
        return ','.join([self.name, str(self.market), '%s odd' % (not self.is_odd and 'not' or '')])


class ExtBet(models.Model):
    """Ссылки на архивы ставок, ключ и букмейкер
    """
    class Meta:
        unique_together = ('ext', 'ext_bookmaker')
    ext = models.CharField(max_length=30, verbose_name='Внешний ключ', default='')
    ext_bookmaker = models.CharField(max_length=6, verbose_name='from 1x2', default='')


class ArchiveOdd(models.Model):
    ext = models.ForeignKey(ExtBet, null=False, default=0)
    odd = models.FloatField(verbose_name='Ставка')
    change = models.FloatField(verbose_name='Изменение')
    dt = models.DateTimeField(verbose_name='Дата-время')


class Bet(models.Model):
    class Meta:
        unique_together = ('event', 'is_average', 'bookmaker', 'market_parameter', 'ext')

    event = models.ForeignKey(Event, null=True, verbose_name='Событие')
    is_average = models.BooleanField(default=False, verbose_name='Среднее?')
    bid = models.DecimalField(decimal_places=2, max_digits=6, default=0, verbose_name='Значение')
    bid_text = models.CharField(default='', max_length=20, verbose_name='Представление')
    bookmaker = models.ForeignKey(Bookmaker, null=True, verbose_name='Букмейкер')
    market_parameter = models.ForeignKey(MarketParameter, null=True, verbose_name='Параметр')
    ext = models.ForeignKey(ExtBet, null=True)

    def __str__(self):
        return ', '.join([
            str(self.event),
            self.ext and 'Ext: %s' % self.ext or '',
            str(self.market_parameter),
            '%s average' % (not self.is_average and 'not' or ''),
            '%s: %s' % (str(self.bookmaker), str(self.bid)),
        ])


class MatchResult(models.Model):
    class Meta:
        unique_together = ('event', 'team')
    event = models.ForeignKey(Event, null=True, verbose_name='Событие')
    team = models.ForeignKey(Team, null=True, verbose_name='Команда')
    pts = models.IntegerField(default=0, verbose_name='Поинты')
    pts_formula2 = models.IntegerField(default=0, verbose_name='Поинты v2')

    def __str__(self):
        return ','.join([
            str(self.event),
            str(self.team),
            'f1: %s' % str(self.pts),
            'f2: %s' % str(self.pts_formula2),
        ])


class CupTeam(models.Model):
    class Meta:
        unique_together = ('cup', 'team')
    cup = models.ForeignKey(Cup, verbose_name='Чемпионат')
    team = models.ForeignKey(Team, verbose_name='Команда')

    def __str__(self):
        return ','.join([
            str(self.cup),
            str(self.team),
        ])


class CupTeamGroup(models.Model):
    cupteam = models.OneToOneField(CupTeam, verbose_name='Команда в чемпионате')
    group_home = models.PositiveIntegerField(verbose_name='Группа дома', default=0)
    group_away = models.PositiveIntegerField(verbose_name='Группа гости', default=0)
    sad_home = models.IntegerField(verbose_name='Просадка дома', default=0)
    sad_away = models.IntegerField(verbose_name='Просадка гости', default=0)

    def __str__(self):
        return ','.join([
            str(self.cupteam),
            str(self.sad_home),
            str(self.sad_away),
            str(self.group_home),
            str(self.group_away),
        ])


class CupTotal(models.Model):
    HOME = 'H'
    AWAY = 'A'
    WHERE = (
        (HOME, 'Дома'),
        (AWAY, 'Гости'),
    )

    RATIO = 'R'
    R12 = 'P'
    RATIOS = (
        (RATIO, 'Соотношение сил'),
        (R12, 'Соотношение ставок'),
    )

    class Meta:
        unique_together = ('cup', 'group', 'where', 'ratio_of', 'ratio')
    cup = models.ForeignKey(Cup, verbose_name='Чемпионат')
    group = models.PositiveSmallIntegerField(default=0, verbose_name='Группа')
    where = models.CharField(max_length=1, choices=WHERE, null=True, verbose_name='Где')
    ratio_of = models.CharField(max_length=1, choices=RATIOS, null=True, verbose_name='Тип соотношения')
    ratio = models.DecimalField(max_digits=6, decimal_places=1, default=0, verbose_name='Значение соотношения')
    value = models.IntegerField(default=0, verbose_name='Значение W')
    matches = models.PositiveSmallIntegerField(default=0, verbose_name='Матчей')

    def __str__(self):
        return ', '.join([
            str(self.cup),
            str(self.group),
            self.get_where_display(),
            self.get_ratio_of_display(),
            str(self.ratio),
            'W: %s' % str(self.value),
            'P: %s' % str(self.matches),
        ])


class ForecastCupCache(models.Model):
    """
    Сохраненные результаты прогнозов
    """
    W_AVG = 'A'
    W_MAX = 'M'
    W_BOOKMAKER = 'B'
    W_TYPES = (
        (W_AVG, 'Average'),
        (W_MAX, 'Maximum'),
        (W_BOOKMAKER, 'Bookmaker'),
    )
    WIN_CHOICES = (
        ('1', 'Дома'),
        ('2', 'Гости')
    )

    class Meta:
        unique_together = ('cup', 'depth', 'win', 'bookmaker', 'w_type')

    cup = models.ForeignKey(Cup, null=True, verbose_name='Чемпионат')
    w_type = models.CharField(choices=W_TYPES, default=W_AVG, max_length=1, null=False)
    bookmaker = models.ForeignKey(Bookmaker, null=True)

    depth = models.IntegerField(verbose_name='Глубина', default=0, null=False)
    win = models.CharField(verbose_name='Победа', default='', choices=WIN_CHOICES, max_length=1, null=False)
    w = models.FloatField(verbose_name='W', null=False)
    games = models.IntegerField(verbose_name='Матчей', default=0)

    def __str__(self):
        return ', '.join([
            str(self.cup),
            str(self.depth),
            self.get_win_display(),
            '%s: %s' % (self.get_w_type_display(), str(self.w)),
        ])


class OptimalForecastHead(models.Model):
    """
    Чемпионаты и их глубины, с которыми они должны быть обсчитаны
    """
    class Meta:
        unique_together = ('cup', 'calculated_depth', )
        verbose_name = 'Оптимальный прогноз'

    cup = models.ForeignKey(Cup, verbose_name='Чемпионат', null=False)
    calculated_depth = models.IntegerField(verbose_name='Расчетная глубина', default=settings.BET_CALCULATED_DEPTH)

    def __str__(self):
        return ', '.join([
            'ID: %s' % self.id,
            str(self.cup),
            str(self.calculated_depth),
        ])

    def get_optimal_forecast(self, type=None, bookmaker=None, attrs=[]):
        # TODO: тесты нужны на констстентность данных.
        if bookmaker:
            kwargs = dict(type=OptimalForecast.W_BOOKMAKER, bookmaker=bookmaker)
        else:
            kwargs = dict(type=type, bookmaker__isnull=True)
        try:
            of_v = self.optimalforecast_set.get(**kwargs)
        except OptimalForecast.MultipleObjectsReturned:
            of_v = self.optimalforecast_set.filter(**kwargs).order_by('-value').first()
        except OptimalForecast.DoesNotExist:
            return 0, 0
        return tuple(getattr(of_v, a) for a in attrs)


class OptimalForecast(models.Model):
    """Результаты расчета поиска оптимального прогноза
    Формируется командой eventsupdate -a optimal-depths -v 3
    """
    W_AVG = 'A'
    W_MAX = 'M'
    W_BOOKMAKER = 'B'
    W_TYPES = (
        (W_AVG, 'Average'),
        (W_MAX, 'Maximum'),
        (W_BOOKMAKER, 'Bookmaker'),
    )

    WIN_HOME = '1'
    WIN_AWAY = '2'
    WIN_SET = 'T'
    WIN_CHOICES = (
        (WIN_HOME, 'Дома'),
        (WIN_AWAY, 'Гости'),
        (WIN_SET, 'Совокупно'),
    )

    class Meta:
        unique_together = ('head', 'type', 'bookmaker')

    head = models.ForeignKey(OptimalForecastHead, null=False)
    type = models.CharField(verbose_name='Тип итога', choices=W_TYPES, max_length=1, null=False, default=W_AVG)
    bookmaker = models.ForeignKey(Bookmaker, null=True)

    depth = models.IntegerField(verbose_name='Глубина', default=0)
    win = models.CharField(verbose_name='Победа', default=WIN_SET, choices=WIN_CHOICES, max_length=1, null=False)
    value = models.FloatField(verbose_name='Итог', default=0)
    season_total = models.FloatField(verbose_name='Итог сезона', default=0)

    def __str__(self):
        return ', '.join([
            str(self.head),
            self.type == self.W_BOOKMAKER and str(self.bookmaker) or self.get_type_display(),
            'Глубина: %s' % str(self.depth),
            'Победа: %s' % self.get_win_display(),
            'Прогноз %s Результат %s' % (str(self.value), str(self.season_total)),
        ])


def get_ctotal_result(kwargs=None):
    """Получить итоговые сырые данные по чемпионату на основании матчей
    :param kwargs:
    :return:
    """
    from .views import w_matches_global, event_bid_global, m_param, m_param_x1, m_param_x2, m_param_x, pinnacle
    if kwargs is None:
        return
    cup = kwargs['cup']
    what_w = kwargs['what_w']
    depth = kwargs['depth']
    w_matches = w_matches_global
    events = Event.objects.filter(cup=cup).order_by('date')
    prev_cup = cup.get_prev_cup()
    event_bid = event_bid_global
    result_events = []
    result_data = []
    for e in events:
        if not (e.ta >= w_matches and e.th >= w_matches):
            continue
        t1 = CupTeam.objects.filter(team=e.team1, cup=prev_cup)
        t2 = CupTeam.objects.filter(team=e.team2, cup=prev_cup)
        if not (t1 and t2):
            continue
        t1_g = t1[0].cupteamgroup.group_home
        t2_g = t2[0].cupteamgroup.group_away

        ratio = e.ratio
        ratio_bid = e.ratio_bid

        l_ctotal = lambda cup_group, t_g, where, ratio_of: CupTotal.objects.filter(
            cup__group=cup_group,
            cup__season_start__lt=cup.season_start,
            group=t_g,
            where=where,
            ratio_of=ratio_of,
            ratio=ratio,
        ).order_by('-cup__season_start')[:depth].aggregate(Sum('value'), Sum('matches'))

        matches_check = lambda x: not (x['matches__sum'] and x['matches__sum'] >= w_matches)

        ctotal_home_ratio = l_ctotal(prev_cup.group, t1_g, CupTotal.HOME, CupTotal.RATIO)
        ctotal_home_ratio_bid = l_ctotal(prev_cup.group, t1_g, CupTotal.HOME, CupTotal.R12)
        ctotal_away_ratio = l_ctotal(prev_cup.group, t2_g, CupTotal.AWAY, CupTotal.RATIO)
        ctotal_away_ratio_bid = l_ctotal(prev_cup.group, t2_g, CupTotal.AWAY, CupTotal.R12)
        if matches_check(ctotal_home_ratio) \
                or matches_check(ctotal_home_ratio_bid) \
                or matches_check(ctotal_away_ratio) \
                or matches_check(ctotal_away_ratio_bid):
            continue

        home_pwr = round(ctotal_home_ratio['value__sum'] / ctotal_home_ratio['matches__sum'])
        home_bid = round(ctotal_home_ratio_bid['value__sum'] / ctotal_home_ratio_bid['matches__sum'])
        away_pwr = round(ctotal_away_ratio['value__sum'] / ctotal_away_ratio['matches__sum'])
        away_bid = round(ctotal_away_ratio_bid['value__sum'] / ctotal_away_ratio_bid['matches__sum'])
        forecast_value = '-'
        who_win = e.get_win()
        bid_x1 = e.get_bid_uni(**m_param[what_w][1])
        bid_x2 = e.get_bid_uni(**m_param[what_w][2])

        w = -event_bid
        get_w = lambda x: (x - 1) * event_bid
        if home_pwr > away_pwr and home_bid > away_bid:
            forecast_value = 1
            if who_win == 1:
                w = get_w(bid_x1)
        if home_pwr < away_pwr and home_bid < away_bid:
            forecast_value = 2
            if who_win == 2:
                w = get_w(bid_x2)
        if forecast_value == '-':
            w = '-'
        result_event = {
            'event': e,
            'team1': e.team1,
            'team2': e.team2,
            'home_grp': t1_g,
            'away_grp': t2_g,
            'home_pwr': home_pwr,
            'home_bid': home_bid,
            'away_pwr': away_pwr,
            'away_bid': away_bid,
            'bid_x1': bid_x1,
            'bid_x2': bid_x2,
            'win': who_win,
            'ratio': e.ratio,
            'ratio_bid': e.ratio_bid,
            'forecast': forecast_value,
            'w': w,
        }

        result_events.append(result_event)

    results_short = [{
                         'win': r['forecast'],
                         'w': float(r['w']),
                     }
                     for r in result_events
                     if str(r['w']) != '-']
    get_total_by_win = lambda x, y: [r[y] for r in results_short if r['win'] == x]
    for num in range(1, 3):
        list_totals = get_total_by_win(num, 'w')
        result_data.append({
            'win': num,
            'w': sum(list_totals),
            'games': len(list_totals),
        })
    return result_data


def get_optimal_forecast(**kwargs):
    """Получаем оптимальные глубины для указанного чемпионата.
    Реализация расчета доступного на листе Статистика документа
    https://docs.google.com/spreadsheets/d/1uMW-pvZM7sq3QgPZKo61aXQ_Jwly3Sei_P_Gnr0CNFs/edit#gid=19610061
    Порядок расчета
    1. Берем три чемпионата перед текущим
    2. Формируем итоги профита и кол-ву игр по каждому чемпионату из 3х и глубине не более максимальной
    3a. Расчитывамем итоги по каждой глубине по всем чемпионатам,
    3b. находим наиболее эффективное значение профита исходя из кол-ва игр и глубину для игр
        дома, в гостях или совокупное значение (joint)
    4. Обновляем кеши данных
    5. Формируем и возвращаем требуемый словарь с результатами

    :param kwargs:
        cup:            Чемпионат
        max_depth:      Максимальная глубина расчета
        calc_w:         Тип расчетных коэффициентов (средние, максимальные, букмейкер: pinacle etc.)
        use_cached:     Если есть по запрашиваемым данным обсчитанные ранее результаты, использовать их
        which_result:   Что вернуть(только данные расчета или подготовить шаблончег TODO окультурить)
    :return:
        dict with keys:
            optimal_forecasts Минимум данные расчета
            либо дополнительные ключи для формирования шаблона (xlsx, template rendering)
    """
    from .views import m_param_x1, m_param_x2, m_param_x, pinnacle
    if not all([arg in kwargs for arg in ['cup', 'cupgroup']]):
        return
    if not 'max_depth' in kwargs:
        kwargs['max_depth'] = settings.BET_CALCULATED_DEPTH
    if not 'use_cached' in kwargs:
        kwargs['use_cached'] = True
    if not 'calc_w' in kwargs:
        kwargs['calc_w'] = ForecastCupCache.W_AVG
    if not 'which_result' in kwargs:
        kwargs['which_result'] = 'all'
    cup = kwargs['cup']
    calc_cups = Cup.objects.filter(
        group=kwargs['cupgroup'],
        season_start__lt=cup.season_start,
    ).order_by('-season_start')[:3]
    if calc_cups.count() < 3:
        return
    results = []
    for c in calc_cups:
        for d in range(1, kwargs['max_depth'] + 1):
            forecast_cached_dict = {}
            cached_qs = ForecastCupCache.objects.none()
            skip_calc = False
            if kwargs['use_cached']:
                forecast_cached_dict = dict(cup=c, depth=d, w_type=kwargs['calc_w'], )

                if kwargs['calc_w'] != ForecastCupCache.W_BOOKMAKER:
                    forecast_cached_dict['bookmaker__isnull'] = True
                else:
                    forecast_cached_dict['bookmaker'] = pinnacle
                cached_qs = ForecastCupCache.objects.filter(**forecast_cached_dict)
                if cached_qs.count() == 2:
                    for c_forecast in cached_qs:
                        fc = {
                            f.name: getattr(c_forecast, f.name)
                            for f in c_forecast._meta.get_fields()
                            }
                        fc['w'] = float(fc['w'])
                        fc['profit_by_bet'] = fc['games'] > 0 and fc['w'] / fc['games'] or 0
                        results.append(fc)
                        skip_calc = True
            if not skip_calc:
                result = get_ctotal_result(kwargs=dict(cup=c, what_w=kwargs['calc_w'], depth=d, ))
                for r in result:
                    r.update({
                        'depth': d,
                        'cup': c,
                        'w_type': kwargs['calc_w'],
                        'bookmaker': kwargs['calc_w'] == ForecastCupCache.W_BOOKMAKER and pinnacle or None,
                        'profit_by_bet': r['games'] > 0 and r['w'] / r['games'] or 0,
                    })
                    forecast_dict = copy.deepcopy(r)
                    forecast_defaults = dict(w=forecast_dict.pop('w'), games=forecast_dict.pop('games'))
                    forecast_dict.pop('profit_by_bet')
                    ForecastCupCache.objects.update_or_create(defaults=forecast_defaults, **forecast_dict)
                results.extend(result)
    for r in results:
        cup_name = r['cup'].name
        r.update({'cup': cup_name})
    data = pd.DataFrame.from_dict(results)
    # функция среднего
    avg_func = lambda x: sum(x) / len(x)
    # применение функции к элементам итерируемого аргумента
    agg_func = lambda y, z: {iz: lambda x: y(x) for iz in z}

    depth_profit = data.groupby(['win', 'depth'], as_index=False).agg(agg_func(avg_func, ['profit_by_bet', 'games']))
    depth_summary = data.groupby(['depth', 'cup'], as_index=False).agg(agg_func(sum, ['w', 'games']))
    depth_summary = depth_summary.assign(profit_by_bet=depth_summary.w / depth_summary.games)
    possible_profit_12 = depth_profit.assign(possible_profit=depth_profit.games * depth_profit.profit_by_bet)
    depth_possible_profit = depth_summary.groupby('depth', as_index=False).agg(
        agg_func(avg_func, ['profit_by_bet', 'games']))
    possible_profit_total = depth_possible_profit.assign(
        possible_profit=depth_possible_profit.games * depth_possible_profit.profit_by_bet)
    possible_profit_total['win'] = 'Итог'
    forecast_df = pd.concat([possible_profit_total, possible_profit_12])
    forecast_dict = forecast_df.to_dict(orient='list')

    d_r = lambda x: '{:8.2f}'.format(x)
    forecast_results = []
    for i in range(0, len(forecast_dict['win'])):
        forecast_results.append({
            'win': forecast_dict['win'][i],
            'depth': forecast_dict['depth'][i],
            'profit_by_bet': d_r(forecast_dict['profit_by_bet'][i]),
            'games': d_r(forecast_dict['games'][i]),
            'possible_profit': d_r(forecast_dict['possible_profit'][i]),
        })
    forecast_totals = forecast_df.loc[
                      lambda forecast_df:
                      forecast_df.possible_profit == forecast_df.possible_profit.max(), :].to_dict(orient='list')
    forecast_totals_list = []

    skip_calc = False
    try_calc = True
    cur_depth = 0
    if forecast_totals['depth']:
        cur_depth = forecast_totals['depth'][0]
    else:
        try_calc = False
    forecast_calc_results = []
    if kwargs['use_cached']:
        forecast_cached_dict = dict(cup=cup, depth=cur_depth, w_type=kwargs['calc_w'], )

        if kwargs['calc_w'] != ForecastCupCache.W_BOOKMAKER:
            forecast_cached_dict['bookmaker__isnull'] = True
        else:
            forecast_cached_dict['bookmaker'] = pinnacle
        cached_qs = ForecastCupCache.objects.filter(**forecast_cached_dict)
        if cached_qs.count() == 2:
            for c_forecast in cached_qs:
                fc = {
                    f.name: getattr(c_forecast, f.name)
                    for f in c_forecast._meta.get_fields()
                    }
                fc['w'] = float(fc['w'])
                fc['profit_by_bet'] = fc['games'] > 0 and fc['w'] / fc['games'] or 0
                forecast_calc_results.append(fc)
                skip_calc = True
    if not skip_calc:
        result = get_ctotal_result(kwargs=dict(cup=cup, what_w=kwargs['calc_w'], depth=cur_depth, ))
        for r in result:
            r.update({
                'depth': cur_depth,
                'cup': cup,
                'w_type': kwargs['calc_w'],
                'bookmaker': kwargs['calc_w'] == ForecastCupCache.W_BOOKMAKER and pinnacle or None,
                'profit_by_bet': r['games'] > 0 and r['w'] / r['games'] or 0,
            })
            forecast_dict = copy.deepcopy(r)
            forecast_defaults = dict(w=forecast_dict.pop('w'), games=forecast_dict.pop('games'))
            forecast_dict.pop('profit_by_bet')
            ForecastCupCache.objects.update_or_create(defaults=forecast_defaults, **forecast_dict)
        forecast_calc_results.extend(result)

    season_total = 0
    optimal_forecasts = []
    for i in range(0, len(forecast_totals['win'])):
        for fcr in forecast_calc_results:
            if not (forecast_totals['depth'][i] == fcr['depth']):
                continue
            if forecast_totals['win'][i] == 'Итог':
                season_total += fcr['w']
            elif forecast_totals['win'][i] == fcr['win']:
                season_total += fcr['w']

        forecast_totals_list.extend([
            {
                'name': 'Победа',
                'value': forecast_totals['win'][i],
            },
            {
                'name': 'Глубина',
                'value': forecast_totals['depth'][i],
            },
            {
                'name': 'Средняя прибыль на ставку',
                'value': d_r(forecast_totals['profit_by_bet'][i]),
            },
            {
                'name': 'Среднее кол-во матчей',
                'value': d_r(forecast_totals['games'][i]),
            },
            {
                'name': 'Потенциальная прибыль',
                'value': d_r(forecast_totals['possible_profit'][i]),
            },
            {
                'name': 'Итог сезона',
                'value': d_r(season_total),
            },
        ])

        if kwargs['max_depth'] == 8 and forecast_totals['depth'][i] > 3\
                and forecast_totals['possible_profit'][i] != 0\
                and season_total != 0:
            of, updated = OptimalForecastHead.objects.update_or_create(cup=cup, calculated_depth=kwargs['max_depth'])
            of_value, updated = OptimalForecast.objects.update_or_create(
                head=of,
                type = kwargs['calc_w'],
                bookmaker=kwargs['calc_w'] == ForecastCupCache.W_BOOKMAKER and pinnacle or None,
                defaults={
                    'value': d_r(forecast_totals['possible_profit'][i]),
                    'season_total': d_r(season_total),
                    'depth': forecast_totals['depth'][i],
                    'win': forecast_totals['win'][i] == 'Итог' and OptimalForecast.WIN_SET or forecast_totals['win'][i],
                }
            )
            optimal_forecasts.append(of)

    for r in results:
        r['profit_by_bet'] = d_r(r['profit_by_bet'])
        r['w'] = d_r(r['w'])

    out_dict = {'optimal_forecasts': optimal_forecasts, }
    if kwargs['which_result'] == 'all':
        out_dict.update({
            'forecast_results': forecast_results,
            'forecast_totals_list': forecast_totals_list,
            'results': results,

        })
    return out_dict
