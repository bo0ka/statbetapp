var request = new XMLHttpRequest();
if (!request) {
    alert('Ошибка инициализации XMLHttpRequest')
}

function updateSelect(select, data) {
    select.find('option').remove();
    for (var k in data) {
        select.append($('<option value="'+data[k]['id']+'">'+data[k]['name']+'</option>'));
    }
}

function updatePage() {
    if (request.readyState == 4) {
        if (request.status == 200) {
            var data = JSON.parse(request.responseText);
            updateSelect($('select[name=cup]'), data);
        } else {
            alert('Error: ' +request.status);
        }
    }
}

function getCup() {
    var cupgroup_id = $('#id_cupgroup').val();
    var url = '/cups/'+cupgroup_id+'/';
    request.open('GET', url, true);
    request.onreadystatechange = updatePage;
    request.send(null);
}