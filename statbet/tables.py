import django_tables2 as tables

from django_filters import FilterSet
from django_tables2.export.views import ExportMixin
from django_tables2 import A

from .models import CupTotal


VALUES_ATTRS = {
        'tf': {
            'class': 'text-right',
        },
        'td': {
            'class': 'text-right',
        },
        'th': {
            'class': 'text-right'
        }
    }


class CupTotalTable(tables.Table):
    class Meta:
        model = CupTotal
        template = 'django_tables2/bootstrap.html'


class CupTotalFilter(FilterSet):
    class Meta:
        model = CupTotal
        fields = ['cup', 'group', 'where']


class ForecastTable(tables.Table):
    """
    Отдельный прогноз
    """
    class Meta:
        template = 'django_tables2/bootstrap.html'

    event = tables.Column()
    team1 = tables.Column()
    team2 = tables.Column()

    home_pwr = tables.Column(verbose_name='Сила дома', attrs=VALUES_ATTRS)
    away_pwr = tables.Column(verbose_name='Сила гости', attrs=VALUES_ATTRS)

    home_bid = tables.Column(verbose_name=' ')
    away_bid = tables.Column()

    home_grp = tables.Column()
    home_grp_current = tables.Column()

    away_grp = tables.Column()
    away_grp_current = tables.Column()

    bid_x1 = tables.Column()
    bid_x = tables.Column()
    bid_x2 = tables.Column()

    bid_x1_max = tables.Column()
    bid_x2_max = tables.Column()

    who_win = tables.Column(verbose_name='Кто победил')

    ratio = tables.Column(verbose_name='Отношение коэф сил команд')
    ratio_bid = tables.Column(verbose_name='Отношение средних коэф букмейкеров')

    forecast = tables.Column()
    w = tables.Column()
    w_max = tables.Column()
    w_pin = tables.Column(verbose_name='W Pinnacle')


class ForecastTotalTable(tables.Table):

    class Meta:
        template = 'django_tables2/bootstrap.html'

    who_win = tables.Column()
    w = tables.Column()
    w_max = tables.Column()
    w_pin = tables.Column(verbose_name='W Pinnacle')
    matches = tables.Column()


class OptDepthTable(tables.Table):

    class Meta:
        template = 'django_tables2/bootstrap.html'

    cup = tables.Column(verbose_name='Чемпионат')
    depth = tables.Column(verbose_name='Глубина')
    win = tables.Column(verbose_name='Победа')
    w = tables.Column(
        verbose_name='W',
        attrs=VALUES_ATTRS,
    )
    games = tables.Column(verbose_name='Игр', attrs=VALUES_ATTRS)
    profit_by_bet = tables.Column(
        verbose_name='Прибыль на ставку',
        empty_values=(),
        attrs=VALUES_ATTRS,
        # footer=lambda table: max([x['profit_by_bet'] for x in table.data] or [''])
    )


class ForecastPossibleTable(tables.Table):
    class Meta:
        template = 'django_tables2/bootstrap.html'

    win = tables.Column(verbose_name='Победа')
    depth = tables.Column(verbose_name='Глубина')
    profit_by_bet = tables.Column(verbose_name='Средняя прибыль на ставку', attrs=VALUES_ATTRS,)
    games = tables.Column(verbose_name='Среднее кол-во матчей', attrs=VALUES_ATTRS,)
    possible_profit = tables.Column(
        attrs=VALUES_ATTRS,
        verbose_name='Потенциальная прибыль',
        empty_values=(),
        # footer=lambda table: max([x['possible_profit'] for x in table.data] or [''])
    )


class ForecastPossibleResultsTable(tables.Table):
    class Meta:
        template = 'django_tables2/bootstrap.html'

    name = tables.Column(verbose_name='Наименование', attrs={
        'td': {
            'strong': True,
        },

    })
    value = tables.Column(verbose_name='Значение', attrs=VALUES_ATTRS)


class ForecastTableView(ExportMixin, tables.SingleTableView):
    table_class = ForecastTable
    template_name = 'django_tables2/bootstrap.html'


class ForecastCacheTable(tables.Table):
    class Meta:
        template = 'django_tables2/bootstrap.html'

    cup = tables.Column(verbose_name='Чемпионат')
    depth = tables.Column(verbose_name='Глубина')
    win = tables.Column(verbose_name='Победа')
    w = tables.Column(verbose_name='W')
    games = tables.Column(verbose_name='Матчей')
    bookmaker = tables.Column(verbose_name='Букмейкер')
    w_type = tables.Column(verbose_name='Тип итога')


class OptimalForecastTable(tables.Table):
    class Meta:
        template = 'django_tables2/bootstrap.html'

    country = tables.Column(verbose_name='Страна', accessor=A('cup.country'))
    cup = tables.Column(verbose_name='Чемпионат', accessor=A('cup.group.name'))
    season = tables.Column(verbose_name='Сезон', accessor=A('cup.season'))
    # calculated_depth = tables.Column(verbose_name='Расчетная глубина')
    depth = tables.Column(verbose_name='Глубина')
    win = tables.Column(verbose_name='Победа')
    value = tables.Column(verbose_name='Итог', attrs=VALUES_ATTRS)
    season_total = tables.Column(verbose_name='Итог сезона', attrs=VALUES_ATTRS)


class OptimalForecastValueTable(tables.Table):
    class Meta:
        template = 'django_tables2/bootstrap.html'

    country = tables.Column(verbose_name='Страна', accessor=A('head.cup.country'))
    cup = tables.Column(verbose_name='Чемпионат', accessor=A('head.cup.group.name'))
    season = tables.Column(verbose_name='Сезон', accessor=A('head.cup.season'))

    depth = tables.Column(verbose_name='Глубина')
    win = tables.Column(verbose_name='Победа')
    value = tables.Column(verbose_name='Итог', attrs=VALUES_ATTRS)
    season_total = tables.Column(verbose_name='Итог сезона', attrs=VALUES_ATTRS)


class ForecastEventTable(tables.Table):

    class Meta:
        template = 'django_tables2/bootstrap.html'

    event = tables.Column(verbose_name='ID')
    year = tables.Column(verbose_name='Год', accessor=A('event.date.year'))
    month = tables.Column(verbose_name='Месяц', accessor=A('event.date.month'))
    day = tables.Column(verbose_name='День', accessor=A('event.date.day'))
    country = tables.Column(verbose_name='Страна', accessor=A('event.country'))
    cup = tables.Column(verbose_name='Чемпионат', accessor=A('event.cup.name'))
    season = tables.Column(verbose_name='Сезон', accessor=A('event.cup.season'))

    team1 = tables.Column(verbose_name='Дома')
    team2 = tables.Column(verbose_name='Гости')
    who_win = tables.Column(verbose_name='Результат')
    score = tables.Column(verbose_name='Счет', accessor=A('event.score'))
    ratio = tables.Column(verbose_name='Отношение сил', accessor=A('event.ratio'))
    ratio_bid = tables.Column(verbose_name='Отношение коэффициентов', accessor=A('event.ratio_bid'))
    home_grp = tables.Column(verbose_name='Группа Дома')
    away_grp = tables.Column(verbose_name='Группа Гости')

    bid_x1 = tables.Column(verbose_name='Ср 1', attrs=VALUES_ATTRS)
    bid_x = tables.Column(verbose_name='Ср X', attrs=VALUES_ATTRS)
    bid_x2 = tables.Column(verbose_name='Ср 2', attrs=VALUES_ATTRS)

    forecast_avg = tables.Column(verbose_name='Прогноз')
    win_avg = tables.Column(verbose_name='Тип анализа')
    depth_avg = tables.Column(verbose_name='Глубина', attrs=VALUES_ATTRS)
    total_avg = tables.Column(verbose_name='Итог', attrs=VALUES_ATTRS)

    bid_x1_max = tables.Column(verbose_name='Max 1', attrs=VALUES_ATTRS)
    bid_x_max = tables.Column(verbose_name='Max X', attrs=VALUES_ATTRS)
    bid_x2_max = tables.Column(verbose_name='Max 2', attrs=VALUES_ATTRS)

    forecast_max = tables.Column(verbose_name='Прогноз')
    win_max = tables.Column(verbose_name='Тип анализа')
    depth_max = tables.Column(verbose_name='Глубина', attrs=VALUES_ATTRS)
    total_max = tables.Column(verbose_name='Итог', attrs=VALUES_ATTRS)

    bid_x1_pin = tables.Column(verbose_name='Pin 1', attrs=VALUES_ATTRS)
    bid_x_pin = tables.Column(verbose_name='Pin X', attrs=VALUES_ATTRS)
    bid_x2_pin = tables.Column(verbose_name='Pin 2', attrs=VALUES_ATTRS)

    forecast_pin = tables.Column(verbose_name='Прогноз')
    win_pin = tables.Column(verbose_name='Тип анализа')
    depth_pin = tables.Column(verbose_name='Глубина', attrs=VALUES_ATTRS)
    total_pin = tables.Column(verbose_name='Итог', attrs=VALUES_ATTRS, )

