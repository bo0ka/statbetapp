from copy import deepcopy
from decimal import Decimal, getcontext
from django.conf import settings
from django.db.models import Q, ObjectDoesNotExist, Sum, F
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.utils.timezone import datetime

from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport

from .forms import ForecastForm, OptDepthForm, ForecastCacheForm, OptForecastForm, ForecastEventForm
from .models import CupTotal, Cup, Event, CupTeam, CupGroup, ForecastCupCache, Bookmaker, MarketParameter
from .models import get_optimal_forecast, OptimalForecastHead, OptimalForecast
from .serializers import CupSerializer
from .tables import CupTotalFilter, ForecastTable, ForecastTotalTable, OptDepthTable, ForecastPossibleTable
from .tables import ForecastPossibleResultsTable, ForecastCacheTable, OptimalForecastTable, OptimalForecastValueTable
from .tables import ForecastEventTable

w_matches_global = settings.BET_ACCEPT_GAMES
event_bid_global = settings.BET_EVENT_BID
# hardcode pinnacle
pinnacle = Bookmaker.objects.get(bookmaker_id=25)
m_param_x1 = MarketParameter.objects.get(market__name='1x2 odds', name='1')
m_param_x = MarketParameter.objects.get(market__name='1x2 odds', name='X')
m_param_x2 = MarketParameter.objects.get(market__name='1x2 odds', name='2')
m_param = {
    ForecastCupCache.W_AVG: {
        1: dict(market_param=m_param_x1),
        2: dict(market_param=m_param_x2),
    },
    ForecastCupCache.W_MAX: {
        1: dict(market_param=m_param_x1, avg=False, is_max=True),
        2: dict(market_param=m_param_x2, avg=False, is_max=True),
    },
    ForecastCupCache.W_BOOKMAKER: {
        1: dict(market_param=m_param_x1, avg=False, is_max=False, bookmaker=pinnacle),
        2: dict(market_param=m_param_x2, avg=False, is_max=False, bookmaker=pinnacle),
    },

}


def index(request):
    return redirect('/forecast')


def get_ctotal(**kwargs):
    # ---!!!--- Strange never used ---!!!---
    """
    Direct calculation of the forecast
    """
    # TODO: rewrite using common get_ctotal_results
    prev_cup = kwargs['prev_cup']
    cup = kwargs['cup']
    t_g = kwargs['t_g']
    where = kwargs['where']
    ratio_of = kwargs['ratio_of']
    ratio = kwargs['ratio']
    depth = kwargs['depth']
    return CupTotal.objects.filter(
        cup__group=prev_cup.group,
        cup__season_start__lt=cup.season_start,
        group=t_g,
        where=where,
        ratio_of=ratio_of,
        ratio=ratio,
    ).order_by('-cup__season_start')[:depth].aggregate(Sum('value'), Sum('matches'))


def forecast(request):
    """
    Формирование отчета "Отдельный прогноз"
    :param request:
    :return:
    """
    filter = CupTotalFilter(request.GET, queryset=CupTotal.objects.all())
    table = ForecastTable(Event.objects.none())

    event_bid = 100
    data = {
        'cup': Cup.objects.get(season_start=datetime(year=2016, day=1, month=1), group__name='Bundesliga',
                               country__name='Germany'),
        'depth': 3,
        'calc_date': datetime(year=2017, day=1, month=3).date(),
        'w_matches': 4,
        'just_total': False,
    }

    if request.method == 'POST':
        forecast_form = ForecastForm(request.POST, initial=data)
        if forecast_form.is_valid():
            cleaned_data = forecast_form.cleaned_data
            cup = cleaned_data['cup']
            depth = cleaned_data['depth']
            w_matches = cleaned_data['w_matches']
            just_total = cleaned_data['just_total']
            prev_cup = cup.get_prev_cup()
            events = Event.objects.filter(cup=cup).order_by('date')
            result_events = []
            for e in events:
                if not (e.ta >= w_matches and e.th >= w_matches):
                    continue
                t1 = CupTeam.objects.filter(team=e.team1, cup=prev_cup)
                t2 = CupTeam.objects.filter(team=e.team2, cup=prev_cup)
                if not (t1 and t2):
                    continue
                t1_current = CupTeam.objects.filter(team=e.team1, cup=cup)
                t2_current = CupTeam.objects.filter(team=e.team2, cup=cup)
                t1_g = t1[0].cupteamgroup.group_home
                t2_g = t2[0].cupteamgroup.group_away
                t1_g_current = t1_current[0].cupteamgroup.group_home
                t2_g_current = t2_current[0].cupteamgroup.group_away

                ratio = e.ratio
                l_ctotal = lambda cup_group, t_g, where, ratio_of: CupTotal.objects.filter(
                    cup__group=cup_group,
                    cup__season_start__lt=cup.season_start,
                    group=t_g,
                    where=where,
                    ratio_of=ratio_of,
                    ratio=ratio,
                ).order_by('-cup__season_start')[:depth].aggregate(Sum('value'), Sum('matches'))

                matches_check = lambda x: not (x['matches__sum'] and x['matches__sum'] >= w_matches)

                ctotal_home_ratio = l_ctotal(prev_cup.group, t1_g, CupTotal.HOME, CupTotal.RATIO)
                if matches_check(ctotal_home_ratio):
                    continue
                ctotal_home_ratio_bid = l_ctotal(prev_cup.group, t1_g, CupTotal.HOME, CupTotal.R12)
                if matches_check(ctotal_home_ratio_bid):
                    continue
                ctotal_away_ratio = l_ctotal(prev_cup.group, t2_g, CupTotal.AWAY, CupTotal.RATIO)
                if matches_check(ctotal_away_ratio):
                    continue
                ctotal_away_ratio_bid = l_ctotal(prev_cup.group, t2_g, CupTotal.AWAY, CupTotal.R12)
                if matches_check(ctotal_away_ratio_bid):
                    continue

                home_pwr = round(ctotal_home_ratio['value__sum']/ctotal_home_ratio['matches__sum'])
                home_bid = round(ctotal_home_ratio_bid['value__sum'] / ctotal_home_ratio_bid['matches__sum'])
                away_pwr = round(ctotal_away_ratio['value__sum'] / ctotal_away_ratio['matches__sum'])
                away_bid = round(ctotal_away_ratio_bid['value__sum'] / ctotal_away_ratio_bid['matches__sum'])
                forecast_value = '-'
                who_win = e.get_win()
                bid_x1 = e.get_bid(m_param_x1)
                bid_x2 = e.get_bid(m_param_x2)
                bid_x1_max = e.get_bid(m_param_x1, avg=False, is_max=True)
                bid_x2_max = e.get_bid(m_param_x2, avg=False, is_max=True)
                bid_x1_pin = e.get_bid(m_param_x1, avg=False, is_max=False, bookmaker=pinnacle)
                bid_x2_pin = e.get_bid(m_param_x2, avg=False, is_max=False, bookmaker=pinnacle)
                bid_x = e.get_bid(m_param_x)
                w = -event_bid
                w_max = -event_bid
                w_pin = -event_bid
                get_w = lambda x: (x - 1) * event_bid
                if home_pwr > away_pwr and home_bid > away_bid:
                    forecast_value = 1
                    if who_win == 1:
                        w = get_w(bid_x1)
                        w_max = get_w(bid_x1_max)
                        w_pin = get_w(bid_x1_pin)
                if home_pwr < away_pwr and home_bid < away_bid:
                    forecast_value = 2
                    if who_win == 2:
                        w = get_w(bid_x2)
                        w_max = get_w(bid_x2_max)
                        w_pin = get_w(bid_x2_pin)
                if forecast_value == '-':
                    w = '-'
                    w_max = '-'
                    w_pin = '-'
                result_event = {
                    'event': e,
                    'team1': e.team1,
                    'team2': e.team2,
                    'home_grp': t1_g,
                    'away_grp': t2_g,
                    'home_grp_current': t1_g_current,
                    'away_grp_current': t2_g_current,
                    'home_pwr': home_pwr,
                    'home_bid': home_bid,
                    'away_pwr': away_pwr,
                    'away_bid': away_bid,
                    'bid_x1': bid_x1,
                    'bid_x': bid_x,
                    'bid_x2': bid_x2,
                    'bid_x1_max': bid_x1_max,
                    'bid_x2_max': bid_x2_max,
                    'who_win': who_win,
                    'ratio': e.ratio,
                    'ratio_bid': e.ratio_bid,
                    'forecast': forecast_value,
                    'w': w,
                    'w_max': w_max,
                    'w_pin': w_pin,
                }

                result_events.append(result_event)

            if not just_total:
                table = ForecastTable(result_events)
                RequestConfig(request).configure(table)

                export_format = request.GET.get('_export', 'xlsx')
                do_export = 'export' in request.GET
                if TableExport.is_valid_format(export_format) and do_export:
                    exporter = TableExport(export_format, table)
                    return exporter.response('table.{}'.format(export_format))
            else:
                results_short = [{
                                     'who_win': r['forecast'],
                                     'w': Decimal(r['w']),
                                     'w_max': Decimal(r['w_max']),
                                     'w_pin': Decimal(r['w_pin']),
                                 }
                                 for r in result_events
                                 if str(r['w']) != '-']
                get_total_by_win = lambda x, y: [r[y] for r in results_short if r['who_win'] == x]
                result_data = []
                for num in range(1, 3):
                    list_totals = get_total_by_win(num, 'w')
                    list_totals_max = get_total_by_win(num, 'w_max')
                    list_totals_pin = get_total_by_win(num, 'w_pin')
                    result_data.append({
                        'who_win': num,
                        'w': sum(list_totals),
                        'w_max': sum(list_totals_max),
                        'w_pin': sum(list_totals_pin),
                        'matches': len(list_totals),
                    })
                table = ForecastTotalTable(result_data)
                RequestConfig(request).configure(table)
    else:
        forecast_form = ForecastForm(initial=data)
    return render(request, 'statbetapp/forecast.html', {
        'forecast_form': forecast_form,
        'forecast_total': table,
    })


def opt_depth(request):
    """Finding optimal forecast depth n him total
    """
    # TODO: use data view like in pandas, not like table-row
    e_none = Event.objects.none()
    table = OptDepthTable(e_none)
    forecast_table = ForecastPossibleTable(e_none)
    forecast_totals_table = ForecastPossibleResultsTable(e_none)
    cup_group = CupGroup.objects.get(name='Bundesliga', country__name='Germany')
    cup = None
    show_tables = False
    initial_data = {
        'cupgroup': cup_group,
        'max_depth': 8,
        'cup': Cup.objects.get(group=cup_group, season_start=datetime(year=2016, day=1, month=1)),
        'use_cached': True,
    }

    if request.method == 'POST':
        optdepth_form = OptDepthForm(request.POST, initial=initial_data)
        if optdepth_form.is_valid():
            show_tables = True
            cleaned_data = optdepth_form.cleaned_data
            cup = cleaned_data['cup']
            fr = get_optimal_forecast(which_result='all', **cleaned_data)
            forecast_table = ForecastPossibleTable(fr['forecast_results'])
            forecast_totals_table = ForecastPossibleResultsTable(fr['forecast_totals_list'])
            table = OptDepthTable(fr['results'])
            table.order_by = ['depth', 'win']
            RequestConfig(request, paginate={'per_page': 200}).configure(forecast_totals_table)
            RequestConfig(request, paginate={'per_page': 200}).configure(forecast_table)
            RequestConfig(request, paginate={'per_page': 200}).configure(table)
    else:
        optdepth_form = OptDepthForm(initial=initial_data)

    return render(request, 'statbetapp/optdepth.html', {
        'optdepth_form': optdepth_form,
        'optdepth_table': table,
        'forecast_table': forecast_table,
        'forecast_totals': forecast_totals_table,
        'cup': cup,
        'show_tables': show_tables,
    })


def cache_clear(request):
    """Simple view to delete all cached forecasts
    """
    alert_message = ''
    if request.method == 'POST':
        delete_count, delete_result = ForecastCupCache.objects.all().delete()
        alert_message = 'Кеши успешно очищены: %s' % str(delete_result)
    return render(request, 'statbetapp/cacheclear.html', {
            'alert_message': alert_message,
        })


def forecast_cache(request):
    """Формирование отчета по списку кешированных прогнозов
    """
    fc = ForecastCupCache.objects.all()
    forecast_cache = ForecastCacheTable(fc)
    forecast_cache.order_by = ['country']
    forecast_cache_form = ForecastCacheForm()
    RequestConfig(request, paginate={'per_page': 200}).configure(forecast_cache)
    return render(request, 'statbetapp/forecastcache.html', {
        'forecast_cache': forecast_cache,
        'forecast_cache_form': forecast_cache_form,
    })


def optimal_forecast(request, ):
    """Формирование отчета по списку оптимальных кешированных прогнозов
    """
    opt_fc_table = OptimalForecastValueTable([])
    if request.method == 'POST':
        opt_forecast_form = OptForecastForm(request.POST)
        if opt_forecast_form.is_valid():
            cleaned_data = opt_forecast_form.cleaned_data
            # if cleaned_data['calc_w'] == ForecastCupCache.W_AVG:
            #     opt_fc = OptimalForecast.objects.all().order_by('cup')  # filter(value__gt=0).order_by('-value')
            #     opt_fc_table = OptimalForecastTable(opt_fc)
            # else:
            opt_fc = OptimalForecast.objects.filter(
                type=cleaned_data['calc_w']
            ).order_by(
                'head__cup'
            )  # filter(value__gt=0).order_by('-value')
            opt_fc_table = OptimalForecastValueTable(opt_fc)
    else:
        opt_forecast_form = OptForecastForm()
        opt_fc = OptimalForecastHead.objects.all().order_by('cup')
        opt_fc_table = OptimalForecastTable(opt_fc)

    RequestConfig(request, paginate={'per_page': 100000}).configure(opt_fc_table)
    return render(request, 'statbetapp/optforecast.html', {
        'opt_forecast': opt_fc_table,
        'opt_forecast_form': opt_forecast_form,
    })


@csrf_exempt
def cups(request, group_pk):
    """retrieve cups list by cup group pk
    """
    cup_objects = Cup.objects.filter(group_id=group_pk)
    if not cup_objects:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = CupSerializer(cups, many=True)
        return JsonResponse(serializer.data, safe=False)


def forecast_events(request):
    """Формирование отчета Метод 2(Лог по году)
    https://docs.google.com/spreadsheets/d/1WcyuCRac_-R1fY-ZLjMV2j6Q3e_FOY8GbC88j3XWix4/edit#gid=921830024
    Список событий с результатами прогнозов
    """

    def get_w(x, y):
        """
        Получить значение итога
        :param x: ключ словаря со значением ставки
        :param y: прогноз на команду
        :return: значение расчета ставки
        """
        if not y:
            return y
        if who_win == y:
            if bids[who_win][x]:
                return (bids[who_win][x] - 1) * event_bid
            else:
                return 0
        return -event_bid

    # filter = CupTotalFilter(request.GET, queryset=CupTotal.objects.all())
    table = ForecastEventTable(Event.objects.none())

    event_bid = settings.BET_EVENT_BID
    data = {
        'start_date': datetime(year=2017, day=1, month=3).date(),
        'end_date': datetime(year=2017, day=1, month=4).date(),

    }

    if request.method == 'POST':
        forecast_form = ForecastEventForm(request.POST, initial=data)
        if not forecast_form.is_valid():
            return render(request, 'statbetapp/forecast.html', {
                'forecast_form': forecast_form,
                'forecast_total': table,
            })

        cleaned_data = forecast_form.cleaned_data
        start_date = cleaned_data['start_date']
        end_date = cleaned_data['end_date']
        if not end_date:
            end_date = start_date.replace(month=end_date.month + 1)
        result_events = []

        events = Event.objects.filter(date__gte=start_date, date__lt=end_date).order_by('date')

        for e in events:
            cup = e.cup

            if not (e.ta >= settings.BET_ACCEPT_GAMES and e.th >= settings.BET_ACCEPT_GAMES):
                continue
            t1_g = e.get_current_group(1)
            t2_g = e.get_current_group(2)

            defaults = {'depth': 0, 'win': None, 'forecast': None, 'total': 0, }
            opt_values = {
                'avg': deepcopy(defaults),
                'max': deepcopy(defaults),
                'pin': deepcopy(defaults),
            }
            try:
                opt_fc_head = OptimalForecastHead.objects.get(cup=cup, calculated_depth=settings.BET_CALCULATED_DEPTH)
            except OptimalForecastHead.DoesNotExist:
                opt_fc_head = None
            a, m, p = opt_values['avg'], opt_values['max'], opt_values['pin']
            dw = ['depth', 'win', ]
            if opt_fc_head:
                a.update(dict(zip(dw, opt_fc_head.get_optimal_forecast(type=OptimalForecast.W_AVG, attrs=dw))))
                m.update(dict(zip(dw, opt_fc_head.get_optimal_forecast(type=OptimalForecast.W_MAX, attrs=dw))))
                p.update(dict(zip(dw, opt_fc_head.get_optimal_forecast(bookmaker=pinnacle, attrs=dw))))
                a['forecast'] = e.get_forecast(a['depth'], team_groups=(t1_g, t2_g), type=a['win'])
                m['forecast'] = e.get_forecast(m['depth'], team_groups=(t1_g, t2_g), type=m['win'])
                p['forecast'] = e.get_forecast(p['depth'], team_groups=(t1_g, t2_g), type=p['win'])

            who_win = e.get_win()

            bid_x1 = e.get_bid(m_param_x1)
            bid_x = e.get_bid(m_param_x)
            bid_x2 = e.get_bid(m_param_x2)

            bid_x1_max = e.get_bid(m_param_x1, avg=False, is_max=True)
            bid_x_max = e.get_bid(m_param_x, avg=False, is_max=True)
            bid_x2_max = e.get_bid(m_param_x2, avg=False, is_max=True)

            bid_x1_pin = e.get_bid(m_param_x1, avg=False, is_max=False, bookmaker=pinnacle)
            bid_x_pin = e.get_bid(m_param_x, avg=False, is_max=False, bookmaker=pinnacle)
            bid_x2_pin = e.get_bid(m_param_x2, avg=False, is_max=False, bookmaker=pinnacle)

            bids = {
                1: {
                    'avg': bid_x1,
                    'max': bid_x1_max,
                    'pin': bid_x1_pin,
                    },
                2: {
                    'avg': bid_x2,
                    'max': bid_x2_max,
                    'pin': bid_x2_pin,
                    },
            }

            a['total'] = get_w('avg', a['forecast'])
            m['total'] = get_w('max', m['forecast'])
            p['total'] = get_w('pin', p['forecast'])

            result_event = {
                'event': e,
                'team1': e.team1,
                'team2': e.team2,
                'home_grp': t1_g,
                'away_grp': t2_g,

                'bid_x1': bid_x1,
                'bid_x': bid_x,
                'bid_x2': bid_x2,
                'bid_x1_max': bid_x1_max,
                'bid_x_max': bid_x_max,
                'bid_x2_max': bid_x2_max,
                'bid_x1_pin': bid_x1_pin,
                'bid_x_pin': bid_x_pin,
                'bid_x2_pin': bid_x2_pin,
                'who_win': who_win,

                'depth_avg': a['depth'],
                'depth_max': m['depth'],
                'depth_pin': p['depth'],
                'win_avg': a['win'],
                'win_max': m['win'],
                'win_pin': p['win'],
                'forecast_avg': a['forecast'],
                'forecast_max': m['forecast'],
                'forecast_pin': p['forecast'],
                'total_avg': a['total'],
                'total_max': m['total'],
                'total_pin': p['total'],
                                
            }

            result_events.append(result_event)

        table = ForecastEventTable(result_events)
        if 'report' in request.POST:
            RequestConfig(request, paginate={'per_page': 100000}).configure(table)
        elif 'export' in request.POST:
            export_format = request.POST.get('_export', 'xlsx')
            if TableExport.is_valid_format(export_format):
                exporter = TableExport(export_format, table)
                return exporter.response('opt_forecast.{}'.format(export_format))

    else:
        forecast_form = ForecastEventForm(initial=data)
    return render(request, 'statbetapp/forecast.html', {
        'forecast_form': forecast_form,
        'forecast_total': table,
    })
