from django.contrib import admin

from import_export.admin import ImportExportModelAdmin

from .models import Country, Cup, CupGroup, Team, Event, Sport, Stage, Market, MarketParameter, Bookmaker, Bet
from .models import MatchResult, CupTeam, CupTeamGroup, CupTotal, ForecastCupCache, OptimalForecast, ArchiveOdd
from .resources import EventResource, CupTotalResource


class EventExportImportAdmin(ImportExportModelAdmin):
    resource_class = EventResource
    list_display = ('event_id', 'country', 'cup', 'team1', 'team2', 'date', 'score', 'paritial_score', 'th', 'ta',
                    'ratio', 'ratio_bid')
    search_fields = ('event_id', 'country__name', '^cup__name', 'team1__name', 'team2__name', 'cup__season_start')
    list_filter = ('cup__season', 'country', 'cup__group', 'stage')
    ordering = ('country__name', 'cup__group__name', 'date')


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'country')
    list_filter = ('country', )
    search_fields = ('name', )


class CupGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', )
    list_filter = ('country',)
    search_fields = ('name', 'country__name')
    ordering = ('country__name', 'name')


class CupAdmin(admin.ModelAdmin):
    list_display = ('group', 'name', 'country', 'season_start', 'season')
    list_filter = ('season', 'country', 'group')
    search_fields = ('group__name', 'country__name')
    ordering = ('country__name', 'group__name', 'name', 'season_start')


class MarketParametrAdmin(admin.ModelAdmin):
    list_display = ('name', 'market', 'is_odd', 'order')
    list_filter = ('market', 'is_odd')
    search_fields = ('name', 'market__name')
    ordering = ('market', 'order')


class BetAdmin(admin.ModelAdmin):
    list_display = ('event', 'is_average', 'bid', 'bookmaker', 'market_parameter')
    search_fields = ('event__event_id', 'event__cup__name')
    # list_editable = ('bid', )
    ordering = ('event', 'market_parameter__market', 'is_average', 'bookmaker', )


class ArchiveOddAdmin(admin.ModelAdmin):
    list_display = ('bet.event', 'bet.ext', 'bet.is_average', 'bet.bookmaker', 'bet.market_parameter', 'odd', 'change', 'dt')
    search_fields = ('bet__event__event_id', 'bet__event__cup__name', 'bet__ext')
    ordering = ('bet', 'bet__market_parameter__market', 'bet__is_average', 'bet__bookmaker', 'dt')


class BookmakerAdmin(admin.ModelAdmin):
    list_display = ('name', 'bookmaker_id', 'url')
    search_fields = ('name', )
    ordering = ('name', )


class CupTeamAdmin(admin.ModelAdmin):
    list_display = ('cup', 'team')
    list_filter = ('cup__season', 'cup__group')
    search_fields = ('cup__name', 'team__name', 'cup__country__name')
    ordering = ('cup__group__name', 'cup__name', 'team__name')


class MatchResultAdmin(admin.ModelAdmin):
    list_display = ('event', 'team', 'pts', 'pts_formula2')
    list_filter = ('event__cup__season', 'event__country', 'event__cup__group', 'event__stage', 'team')
    search_fields = ('event__event_id', 'team__name')


class CupTeamGroupAdmin(admin.ModelAdmin):
    list_display = ('cupteam', 'group_home', 'group_away', 'sad_home', 'sad_away')
    list_filter = ('cupteam__cup__season', 'cupteam__cup__group', )
    search_fields = ('^cupteam__cup__group__name', '^cupteam__cup__name', 'cupteam__team__name', 'cupteam__cup__season_start')
    ordering = ('cupteam__cup__group__name', 'group_home', 'group_away', 'cupteam__team__name')


class CupTotalAdmin(ImportExportModelAdmin):
    resource_class = CupTotalResource
    list_display = ('cup', 'group', 'where', 'ratio_of', 'ratio', 'value', 'matches')
    list_filter = ('where', 'ratio_of', 'group', 'cup__season', 'cup__group')
    search_fields = ('^cup__group__name', )
    ordering = ('cup__name', 'where', 'ratio_of')


class ForecastCupCacheAdmin(admin.ModelAdmin):
    list_display = ('cup', 'depth', 'win', 'w_type', 'bookmaker', 'games', 'w')
    list_filter = ('w_type', 'depth', 'win', 'cup__season', 'cup__group')
    search_fields = ('^cup__group__name', '^cup__name', 'cup__season_start')


admin.site.register(Sport)
admin.site.register(Country)
admin.site.register(CupGroup, CupGroupAdmin)
admin.site.register(Cup, CupAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Stage)

admin.site.register(Market)
admin.site.register(MarketParameter, MarketParametrAdmin)
admin.site.register(Bookmaker, BookmakerAdmin)
admin.site.register(Bet, BetAdmin)
# admin.site.register(ArchiveOdd, ArchiveOddAdmin)
admin.site.register(Event, EventExportImportAdmin)

admin.site.register(CupTeam, CupTeamAdmin)
admin.site.register(CupTeamGroup, CupTeamGroupAdmin)
admin.site.register(MatchResult, MatchResultAdmin)

admin.site.register(CupTotal, CupTotalAdmin)
admin.site.register(ForecastCupCache, ForecastCupCacheAdmin)
admin.site.register(OptimalForecast)
