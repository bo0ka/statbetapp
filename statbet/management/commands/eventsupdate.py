import re
import numpy
import time

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.db.models import Q, ObjectDoesNotExist
from django.utils.timezone import datetime

from statbet.models import Cup, Event, CupGroup, MatchResult, CupTeam, MarketParameter
from statbet.models import CupTeamGroup, CupTotal, get_optimal_forecast, ForecastCupCache
from statbet.utils import progress_bar, load_common, update_common


class Command(BaseCommand):
    help = 'Обновляет данные по: победам команд, сезонам, мячям, победам в сезонах, результатам матчей,' \
           'спискам команд в чемпионатах, распределению максимумов просадок команд и деление на группы в чемпионатах,' \
           'обновление, соотношений, формирование сводных данных по группам'

    def add_arguments(self, parser):
        parser.add_argument(
            '-a', '--action', dest='action', action='append', default=[],
            help='Типы обновлений: cup-seasons, team-season-wins, match-results, match-results-formula2, cup-teams, '
                 'cup-team-group, cup-team-group-div, event-ratios, cup-totals, optimal-depths',
        )

        parser.add_argument(
            '-p', '--param', dest='param', action='append', default=[],
            help='Параметры',
        )

    def handle(self, *args, **options):
        self.verbosity = options['verbosity']
        self.actions = options['action']
        self.params = options['param']
        self.mparam_x1 = MarketParameter.objects.get(market__name='1x2 odds', name='1')
        self.mparam_x2 = MarketParameter.objects.get(market__name='1x2 odds', name='2')
        self.event_bid = settings.BET_EVENT_BID

        if not self.actions:
            raise CommandError('Не задан action параметр')

        # обновление сезонов
        if 'cup-seasons' in self.actions:
            self.update_cup_seasons()

        # победы в сезонах
        if 'team-season-wins' in self.actions:
            self.update_team_season_wins()

        # результаты матчей по формуле 1
        if 'match-results' in self.actions:
            with transaction.atomic():
                self.match_results()

        # результаты матчей по формуле 2
        if 'match-results-formula2' in self.actions:
            with transaction.atomic():
                self.match_results_formula2()

        # спискам команд в чемпионатах
        if 'cup-teams' in self.actions:
            self.cup_teams()

        # распределению максимумов просадок команд
        if 'cup-team-group' in self.actions:
            # bundesliga_group = CupGroup.objects.get(id=202)
            # season = datetime(year=2014, month=1, day=1)
            # bundesliga = bundesliga_group.cup_set.get(season_start=season)
            # self.cup_team_group(bundesliga)

            # germany_cupgroups = CupGroup.objects.filter(country__name='Germany')
            # for cg in germany_cupgroups:
            cupgroups = CupGroup.objects.all() # filter(country__name='Germany')
            cupgroupstotal = cupgroups.count()
            progress_bar(0, cupgroupstotal, prefix='Progress cup groups:', suffix='Complete', length=50)
            for i, cg in enumerate(cupgroups):
                if i % 10 == 0:
                    progress_bar(i + 1, cupgroupstotal, prefix='Progress cup groups:', suffix='Complete', length=50)

                if self.verbosity >= 2:
                    self.stdout.write('Расчет просадок для "%s"' % cg)
                for c in cg.cup_set.all():
                    self.cup_team_group(c)
                if self.verbosity >= 2:
                    self.stdout.write('Расчет просадок для "%s" завершен' % cg)

        # деление команд на группы в чемпионатах
        if 'cup-team-group-div' in self.actions:
            # bundesliga_group = CupGroup.objects.get(id=202)
            # season = datetime(year=2014, month=1, day=1)
            # bundesliga = bundesliga_group.cup_set.get(season_start=season)
            # self.cup_team_group_div(bundesliga)

            # germany_cupgroups = CupGroup.objects.filter(country__name='Germany')
            # for cg in germany_cupgroups:
            cupgroups = CupGroup.objects.all()
            cupgroupstotal = cupgroups.count()
            progress_bar(0, cupgroupstotal, prefix='Progress cup groups:', suffix='Complete', length=50)
            for i, cg in enumerate(cupgroups):
                if i % 10 == 0:
                    progress_bar(i + 1, cupgroupstotal, prefix='Progress cup groups:', suffix='Complete', length=50)

                if self.verbosity >= 2:
                    self.stdout.write('Расчет групп команд для "%s"' % cg)
                for c in cg.cup_set.all():
                    self.cup_team_group_div(c)
                if self.verbosity >= 2:
                    self.stdout.write('Расчет групп команд для "%s" завершен' % cg)

        # обновление соотношений
        if 'event-ratios' in self.actions:
            calc_all = False
            if 'calculate-all' in self.params:
                calc_all = True
            #germany_events = Event.objects.filter(country__name='Germany')
            #for e in germany_events:
            events = Event.objects.all()
            totalevents = events.count()
            progress_bar(0, totalevents, prefix='Progress events:', suffix='Complete', length=50)
            for i, e in enumerate(events):
                if i % 100 == 0:
                    progress_bar(i + 1, totalevents, prefix='Progress events:', suffix='Complete', length=50)
                changed = False
                if calc_all or not e.ratio_bid != 0:
                    devide = e.get_bid(self.mparam_x1)
                    devider = e.get_bid(self.mparam_x2)
                    if devide and devider:
                        e.ratio_bid = round(devide/devider, 1)
                        changed = True

                if calc_all or not e.ratio != 0:
                    devide = e.team_formula1(e.team1)
                    devider = e.team_formula1(e.team2)
                    if devide and devider:
                        e.ratio = round(devide / devider, 1)
                        changed = True

                if changed:
                    e.save()

        # формирование итоговых данных по чемпинатам-группам
        if 'cup-totals' in self.actions:
            # germany_cupgroups = CupGroup.objects.filter(country__name='Germany')
            cupgroups = CupGroup.objects.all()
            cupgroupstotal = cupgroups.count()
            progress_bar(0, cupgroupstotal, prefix='Progress cup groups:', suffix='Complete', length=50)
            for i, cg in enumerate(cupgroups):
                if i % 10 == 0:
                    progress_bar(i + 1, cupgroupstotal, prefix='Progress cup groups:', suffix='Complete', length=50)
                #bundesliga_group = CupGroup.objects.get(id=202)
                bundesligas = cg.cup_set.all()
                # season = datetime(year=2014, month=1, day=1)
                # bundesligas = bundesligas.filter(season_start=season)
                #self.cup_totals(bundesliga)

                for blc in bundesligas:
                    self.cup_totals(blc)

        # расчет оптимальных глубин
        if 'optimal-depths' in self.actions:
            cups = Cup.objects.all()
            cupscount = cups.count()
            progress_bar(0, cupscount, prefix='Progress optdepths:', suffix='Complete', length=50)
            for i, c in enumerate(cups):
                if i % 10 == 0:
                    progress_bar(i + 1, cupscount, prefix='Progress optdepths:', suffix='Complete', length=50)
                for v_type in dict(ForecastCupCache.W_TYPES).keys():
                    try:
                        get_optimal_forecast(
                            cupgroup=c.group,
                            cup=c,
                            max_depth=8,
                            calc_w=v_type,
                            use_cached=True,
                            which_result='part',
                        )
                    except:
                        print('%s %s' % (c, v_type))


    def cup_totals(self, cup):
        # POWER & BID
        total = []
        # TODO: get prev season will be in a right way
        # prev_cup_start = cup.season_start.replace(year=cup.season_start.year-1)
        # try:
        #     prev_cup = Cup.objects.get(group=cup.group, season_start=prev_cup_start)
        # except ObjectDoesNotExist:
        #     return
        # cts = prev_cup.cupteam_set.all()
        cts = cup.cupteam_set.all()
        if not cts.count():
            return
        events = cup.event_set.all()

        for e in events:
            try:
                match_result = e.matchresult_set.get(team=e.team1)
                ct_home = cts.get(team=e.team1)
                bet_home = e.bet_set.get(market_parameter=self.mparam_x1, is_average=True)
                if match_result.pts > 2:
                    w1_home = round((bet_home.bid - 1) * self.event_bid)
                else:
                    w1_home = -self.event_bid
                g_home = ct_home.cupteamgroup.group_home
            except ObjectDoesNotExist:
                ct_home = None

            try:
                match_result = e.matchresult_set.get(team=e.team2)
                ct_away = cts.get(team=e.team2)
                bet_away = e.bet_set.get(market_parameter=self.mparam_x2, is_average=True)
                if match_result.pts > 2:
                    w2_away = round((bet_away.bid - 1) * self.event_bid)
                else:
                    w2_away = -self.event_bid
                g_away = ct_away.cupteamgroup.group_away
            except ObjectDoesNotExist:
                ct_away = None

            if ct_home:
                # HOME POWER
                total.append({
                    'cup': e.cup,
                    'group': g_home,
                    'where': CupTotal.HOME,
                    'ratio_of': CupTotal.RATIO,
                    'ratio': e.ratio,
                    'value': w1_home,
                })
                # HOME BID
                total.append({
                    'cup': e.cup,
                    'group': g_home,
                    'where': CupTotal.HOME,
                    'ratio_of': CupTotal.R12,
                    'ratio': e.ratio_bid,
                    'value': w1_home,
                })
            if ct_away:
                # AWAY POWER
                total.append({
                    'cup': e.cup,
                    'group': g_away,
                    'where': CupTotal.AWAY,
                    'ratio_of': CupTotal.RATIO,
                    'ratio': e.ratio,
                    'value': w2_away,
                })
                # AWAY BID
                total.append({
                    'cup': e.cup,
                    'group': g_away,
                    'where': CupTotal.AWAY,
                    'ratio_of': CupTotal.R12,
                    'ratio': e.ratio_bid,
                    'value': w2_away,
                })
        res_dict = {}
        for t in total:
            tuple_key = (t['cup'], t['group'], t['where'], t['ratio_of'], t['ratio'])
            if tuple_key not in res_dict:
                old_list = []

            else:
                old_list = res_dict[tuple_key]
            res_dict[tuple_key] = old_list+[{'value': t['value']}]
        for k,v in res_dict.items():
            up_dict = dict(zip(['cup', 'group', 'where', 'ratio_of', 'ratio'], k))
            list_values = [vi['value'] for vi in v]
            up_dict.update(dict(defaults=dict(matches=len(list_values), value=sum(list_values))))
            cup_total = update_common(
                self,
                CupTotal,
                'Обновлен итог: "%s"',
                **up_dict
            )

    def cup_team_group_div(self, cup):
        def get_group(group_list, ctg, where):
            res = 1
            if where == 'home':
                for gv in group_list:
                    if ctg.sad_home > gv:
                        res = 4 - group_list.index(gv)
                        break
                return res
            elif where == 'away':
                for gv in group_list:
                    if ctg.sad_away > gv:
                        res = 4 - group_list.index(gv)
                        break
                return res

        if not (cup.cupteam_set.all() and all([hasattr(ct, 'cupteamgroup') for ct in cup.cupteam_set.all()])):
            return
        np_array = lambda x: numpy.array([getattr(ct.cupteamgroup, x) for ct in cup.cupteam_set.all()])
        np_array_home = np_array('sad_home')
        np_array_away = np_array('sad_away')
        group_values = lambda x:[
            numpy.percentile(x, 25),
            numpy.percentile(x, 50),
            numpy.percentile(x, 75)
        ]
        group_values_home = sorted(group_values(np_array_home), reverse=True)
        group_values_away = sorted(group_values(np_array_away), reverse=True)

        for ct in cup.cupteam_set.all():
            ctg = ct.cupteamgroup

            ctg.group_home = get_group(group_values_home, ctg, 'home')
            ctg.group_away = get_group(group_values_away, ctg, 'away')

            ctg.save()

    def cup_team_group(self, cup):
        for cupteam in cup.cupteam_set.all():
            events_home = Event.objects.filter(cup=cupteam.cup, team1=cupteam.team).order_by('date')
            self.get_cup_team_group_sad(cupteam, events_home, 'home')
            events_away = Event.objects.filter(cup=cupteam.cup, team2=cupteam.team).order_by('date')
            self.get_cup_team_group_sad(cupteam, events_away, 'away')

    def get_cup_team_group_sad(self, cupteam, events, where):
        team = cupteam.team
        events_dict = {}
        bank = 0
        total_sad = 0
        for e in events:
            match_result = e.matchresult_set.get(team=team)
            if not e.bet_set.all():
                continue
            try:
                if where == 'home':
                    bet = e.bet_set.get(market_parameter=self.mparam_x1, is_average=True)
                elif where == 'away':
                    bet = e.bet_set.get(market_parameter=self.mparam_x2, is_average=True)
            except ObjectDoesNotExist:
                continue
            if match_result.pts > 2:
                w1 = round((bet.bid - 1) * self.event_bid)
            else:
                w1 = -self.event_bid
            bank += w1
            banks = [v['bank'] for k, v in events_dict.items()]+[bank, 0]
            # if not banks:
            #     banks = [0]
            sad = abs(max(banks) - bank)
            events_dict[e.date] = {
                'w1': w1,
                'bank': bank,
                'sad': sad,
            }

        max_sad = [v['sad'] for k, v in events_dict.items()]
        if not max_sad:
            max_sad = [0]
        total_sad = max(max_sad)
        defs = {
            'sad_%s' % where: total_sad
        }

        ctg, created = CupTeamGroup.objects.update_or_create(cupteam=cupteam, defaults=defs)
        if not created and self.verbosity >= 2 and where == 'away':
            self.stdout.write('Обновлена просадка: "%s"' % ctg)
        # print('%s: %s' % (team, where))
        # print(events_dict)

    def cup_teams(self):
        cups = Cup.objects.all()
        cupstotal = cups.count()
        progress_bar(0, cupstotal, prefix='Progress cups:', suffix='Complete', length=50)
        for i, c in enumerate(cups):
            if i % 10 == 0:
                progress_bar(i + 1, cupstotal, prefix='Progress cups:', suffix='Complete', length=50)
            teams = []
            for e in Event.objects.filter(cup=c):
                if e.team1 not in teams:
                    teams.append(e.team1)
                if e.team2 not in teams:
                    teams.append(e.team2)
            for t in teams:
                load_common(self, CupTeam, 'Добавлена команда в чемпионат: "%s"', cup=c, team=t)

    def match_results(self):
        events = Event.objects.all()
        totalevents = events.count()
        progress_bar(0, totalevents, prefix='Progress events:', suffix='Complete', length=50)
        for i, e in enumerate(events):
            if i % 100 == 0:
                progress_bar(i + 1, totalevents, prefix='Progress events:', suffix='Complete', length=50)
            pts1 = pts2 = 0
            splited_score = e.score.split(':')
            if len(splited_score) == 2:
                if splited_score[0] > splited_score[1]:
                    pts1 = 3
                elif splited_score[1] > splited_score[0]:
                    pts2 = 3
                else:
                    pts1 = pts2 = 1
            MatchResult.objects.update_or_create(event=e, team=e.team1, defaults={'pts': pts1})
            MatchResult.objects.update_or_create(event=e, team=e.team2, defaults={'pts': pts2})

    def match_results_formula2(self):
        events = Event.objects.all()
        totalevents = events.count()
        progress_bar(0, totalevents, prefix='Progress events:', suffix='Complete', length=50)
        for i, e in enumerate(events):
            if i % 100 == 0:
                progress_bar(i+1, totalevents, prefix='Progress events:', suffix='Complete', length=50)
            pts1 = pts2 = -3
            splited_score = e.score.split(':')
            if len(splited_score) == 2:
                if splited_score[0] > splited_score[1]:
                    pts1 = 3
                elif splited_score[1] > splited_score[0]:
                    pts2 = 3
                else:
                    pts1 = pts2 = 1
            MatchResult.objects.update_or_create(event=e, team=e.team1, defaults={'pts_formula2': pts1})
            MatchResult.objects.update_or_create(event=e, team=e.team2, defaults={'pts_formula2': pts2})

    def update_team_season_wins(self):
        events = Event.objects.all()
        totalevents = events.count()
        progress_bar(0, totalevents, prefix='Progress events:', suffix='Complete', length=50)
        for i, e in enumerate(events):
            e.th = Event.objects.filter(cup=e.cup, date__lt=e.date).filter(
                Q(team1=e.team1) | Q(team2=e.team1)).count()
            e.ta = Event.objects.filter(cup=e.cup, date__lt=e.date).filter(
                Q(team1=e.team2) | Q(team2=e.team2)).count()
            e.save()
            if i % 100 == 0:
                progress_bar(i+1, totalevents, prefix='Progress events:', suffix='Complete', length=50)

    def update_cup_seasons(self):
        cups = Cup.objects.all()
        cupstotal = cups.count()
        progress_bar(0, cupstotal, prefix='Progress cups:', suffix='Complete', length=50)
        for i, cup in enumerate(cups):
            if i % 10 == 0:
                progress_bar(i+1, cupstotal, prefix='Progress cups:', suffix='Complete', length=50)
            years = re.findall('\d{4}', cup.name)
            if len(years) > 0 and len(years) < 3:
                cup.season_start = datetime(year=int(years[0]), month=1, day=1)
                cup.season = '/'.join(years)
                cupgroupname = cup.name.split('/'.join(re.findall('\d{4}', cup.name)))[0].rstrip()
                cup.group = load_common(self, CupGroup, 'Добавлена группа чемпионатов: "%s"', name=cupgroupname, country = cup.country)
                cup.save()
            else:
                self.stdout.write('Cup years update error: %s' % cup)

