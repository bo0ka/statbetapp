import os
import re
import simplejson as json

from decimal import Decimal
from urllib.parse import urlsplit

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import datetime

from statbet.models import Event, Market, MarketParameter, Bookmaker, Bet, ExtBet
from statbet.utils import load_common, update_common

from lxml import etree


class Command(BaseCommand):
    help = 'Загрузка данных коэффициентов из json'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '-i', '--input', default=None, dest='input', help='Импортируемый файл'
        )

        parser.add_argument(
            '-a', '--action', default=None, dest='action', help='Action: avg, bookmaker'
        )

    def handle(self, *args, **options):
        self.input = options['input']
        self.action = options['action']
        if not self.action:
            return
        if not (self.input and os.path.isfile(self.input)):
            raise CommandError('Не верно задан файл "%s"' % self.input)

        self.verbosity = options['verbosity']
        first_line = True
        self.counter = 1
        with open(self.input) as f:
            for line in f:
                if first_line:
                    first_line = False
                    line = line[1:]
                self.row = json.loads(line[:-2])

                if self.row['tab_id'] != '1x2':
                    continue
                self.load_row()
                self.counter += 1
                self.stdout.write('counter: %s; id: %s' % (self.counter, self.row['match_id']))

    def load_row(self):
        if not 'ajax_response' in self.row:
            return
        if not self.row['tab_id'] == '1x2':
            return
        event_id = self.row['match_id']
        self.event = None
        try:
            self.event = Event.objects.get(event_id=event_id)
        except ObjectDoesNotExist:
            if self.verbosity >= 2:
                self.stdout.write('Не найдено событие %s' % event_id)
        self.market = load_common(self, Market, 'Добавлен маркет: "%s"', defaults={'name': self.row['tab_id']},
                                       sysname=self.row['tab_id'])

        self.mparams = MarketParameter.objects.filter(market=self.market, is_odd=True).order_by('order')
        parser = etree.HTMLParser()
        self.root = etree.fromstring('<div>%s</div>' % self.row['ajax_response'], parser)
        if 'avg' in self.action:
            self.load_averages()
        if 'bookmaker' in self.action:
            self.load_by_bookmaker()

    def load_averages(self):
        counter = 0
        for i in self.root.iterfind('.//tfoot/tr[1]/td[@data-odd]'):
            if not i.attrib['data-odd']:
                break
            b, created = Bet.objects.get_or_create(
                event=self.event, is_average=True, market_parameter=self.mparams[counter], bookmaker=None,
                defaults={
                    'bid': float(i.attrib['data-odd']),
                    'bid_text': i.attrib['data-odd'],
                }
            )
            counter += 1
            if created and self.verbosity >= 2:
                self.stdout.write('Добавлена ставка: %s' % b)

    def load_by_bookmaker(self):
        counter = 0
        for tr in self.root.iterfind('.//tbody/tr'):
            counter_td = 0
            self.bookmaker = None
            for td in tr.iterfind('.//td'):
                # url на букмейкера и наименование
                if 'class' in td.attrib and 'over-s-only' in td.attrib['class']:
                    # title
                    bookmaker_url = td.find('.//a')
                    bookmaker_name = bookmaker_url.text
                    data_bid = int(bookmaker_url.attrib['data-bid'])
                    href = bookmaker_url.attrib['href']
                    bookmaker_href = re.match('^/bookmaker/\d+/(.*)$', href).group(1)
                    base_url = "{0.scheme}://{0.netloc}/".format(urlsplit(bookmaker_href))
                    bookmaker, bookmaker_created = Bookmaker.objects.get_or_create(
                        bookmaker_id=data_bid,
                        name=bookmaker_name,
                        defaults=dict(
                            url=base_url,
                        ),

                    )
                    if bookmaker_created and self.verbosity >= 2:
                        self.stdout.write('Добавлен букмейкер: %s' % bookmaker)
                    self.bookmaker = bookmaker
                if 'class' in td.attrib and 'table-main__odds' in td.attrib['class']:
                    span = td.find('.//span')
                    ext_b = None
                    if 'class' in span.attrib and 'table-main__odds--hasarchive' in span.attrib['class']:
                        onclick = span.attrib['onclick']
                        list_tuples = re.findall('\'(\w+)\',\s(\d+)', onclick)
                        if list_tuples:
                            values_tuple = list_tuples[0]
                            ext_b = update_common(
                                self, ExtBet, 'Добавлен ExtBet: "%s"',
                                ext=values_tuple[0],
                                ext_bookmaker=values_tuple[1],
                            )
                    b, created = Bet.objects.update_or_create(
                        event=self.event,
                        is_average=False,
                        market_parameter=self.mparams[counter_td],
                        bookmaker=self.bookmaker,
                        ext=ext_b,
                        defaults={
                            'bid': Decimal(td.attrib['data-odd']),
                            'bid_text': td.attrib['data-odd'],

                        })
                    counter_td += 1
                    if created and self.verbosity >= 2:
                        self.stdout.write('Добавлена ставка: %s' % b)

                    # table-main__odds--hasarchive


