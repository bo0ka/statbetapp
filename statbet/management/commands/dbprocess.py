from django.core import management
import os

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Расчет загруженных данных'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

    def handle(self, *args, **options):
        self.verbosity = options['verbosity']
        self.stdout.write('Распределению максимумов просадок команд')
        management.call_command('eventsupdate', action='cup-team-group', verbosity=self.verbosity)
        self.stdout.write('Деление команд на группы в чемпионатах')
        management.call_command('eventsupdate', action='cup-team-group-div', verbosity=self.verbosity)
        self.stdout.write('Обновление соотношений')
        management.call_command('eventsupdate', action='event-ratios', verbosity=self.verbosity, param='calculate-all')
        self.stdout.write('Формирование итоговых данных по чемпинатам-группам')
        management.call_command('eventsupdate', action='cup-totals', verbosity=self.verbosity)
        self.stdout.write('Расчет оптимальных глубин чемпионатов')
        management.call_command('eventsupdate', action='optimal-depths', verbosity=self.verbosity)
