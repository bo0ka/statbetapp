from django.core import management
import os

from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'Последовательная загрузка из json'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '-i', '--input', default=None, dest='input', help='Путь до папки с импортируемыми файлами'
        )

    def handle(self, *args, **options):
        self.input = options['input']
        if not (self.input and os.path.isdir(self.input)):
            raise CommandError('Не верно задан путь"%s"' % self.input)

        self.verbosity = options['verbosity']
        filename = os.path.join(self.input, 'betexplorer.json')
        self.stdout.write('Загрузка событий из файла: %s' % filename)
        management.call_command('loadevents', verbosity=self.verbosity, input=filename)
        self.stdout.write('Обновление чемпионатов')
        management.call_command('eventsupdate', verbosity=self.verbosity, action='cup-seasons')
        self.stdout.write('Обновление team-season-wins')
        management.call_command('eventsupdate', verbosity=self.verbosity, action='team-season-wins')
        self.stdout.write('Расчет результатов')
        management.call_command('eventsupdate', verbosity=self.verbosity, action='match-results')
        management.call_command('eventsupdate', verbosity=self.verbosity, action='match-results-formula2')
        self.stdout.write('Обновление команд в чемпионатах')
        management.call_command('eventsupdate', verbosity=self.verbosity, action='cup-teams')
        filename = os.path.join(self.input, 'betexplorer_odds.json')
        self.stdout.write('Загрузка средних коэффициентов: %s' % filename)
        management.call_command('loadbets', verbosity=self.verbosity, input=filename, action='avg')
        self.stdout.write('Загрузка коэффициентов букмейкеров: %s' % filename)
        management.call_command('loadbets', verbosity=self.verbosity, input=filename, action='bookmaker')
