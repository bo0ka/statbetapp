import os
import simplejson as json

from django.core.management.base import BaseCommand, CommandError
from django.utils.timezone import datetime

from statbet.models import Stage, Sport, Team, Cup, Event, Country
from statbet.utils import load_common, progress_bar


class Command(BaseCommand):
    help = 'Загрузка данных событий из json'

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '-i', '--input', default=None, dest='input', help='Импортируемый файл'
        )

    def handle(self, *args, **options):
        self.input = options['input']
        if not (self.input and os.path.isfile(self.input)):
            raise CommandError('Не верно задан файл "%s"' % self.input)

        self.verbosity = options['verbosity']

        f = open(self.input)
        data = json.load(f)
        # activate('en')
        datalen = len(data)
        progress_bar(0, datalen, prefix='Progress load events:', suffix='Complete', length=50)
        for i, row in enumerate(data):
            if i % 100 == 0:
                progress_bar(i + 1, datalen, prefix='Progress load events:', suffix='Complete', length=50)
            self.row = row
            self.load_row()

    def load_row(self):
        if not 'cup' in self.row:
            return
        self.event_id = self.row['id']
        if not self.event_id:
            return
        self.stage = None
        if self.row['stage']:
            self.stage = self.load_stage()

        self.country = None
        if self.row['country']:
            self.country = self.load_country()

        self.sport = None
        if self.row['sport_kind']:
            self.sport = self.load_sport()

        self.team1 = None
        if self.row['team1']:
            self.team1 = load_common(self, Team, 'Добавлена команда: "%s"', name=self.row['team1'], country=self.country)

        self.team2 = None
        if self.row['team2']:
            self.team2 = load_common(self, Team, 'Добавлена команда: "%s"', name=self.row['team2'], country=self.country)

        if not (self.team1 and self.team2):
            return
        self.cup = None
        if self.row['cup']:
            self.cup = load_common(self, Cup, 'Добавлена группа событий: "%s"', name=self.row['cup'], country=self.country)

        self.url = self.row['url']
        self.paritial_score = self.row['paritial_score']
        self.score = self.row['total_score']
        self.event_date = datetime.strptime(self.row['event_dt'], '%d.%m.%Y')
        # print(self.event_id)
        if Event.objects.filter(event_id=self.event_id).count() == 0:
            e, created = Event.objects.get_or_create(
                event_id=self.event_id, stage=self.stage, team1=self.team1, team2=self.team2, score=self.score,
                paritial_score=self.paritial_score, cup=self.cup, country=self.country,  url=self.url, sport=self.sport,
                date=self.event_date
            )
            if created and self.verbosity >= 2:
                self.stdout.write('Добавлено событие %s' % e)

    def load_stage(self):
        return load_common(self, Stage, 'Добавлен доп. результат события: "%s"', name=self.row['stage'])

    def load_country(self):
        return load_common(self, Country, 'Добавлена страна: "%s"', name=self.row['country'])

    def load_sport(self):
        return load_common(self, Sport, 'Добавлен вид спорта: "%s"', name=self.row['sport_kind'])

