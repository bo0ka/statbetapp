from django.conf.urls import url, include
from statbet import views
from .models import Cup
from rest_framework import routers, serializers, viewsets


# Serializers define the API representation.
class CupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cup
        fields = ('id', 'name', 'season_start')


# ViewSets define the view behavior.
class CupViewSet(viewsets.ModelViewSet):
    queryset = Cup.objects.all()
    serializer_class = CupSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'cup', CupViewSet)


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^forecast/$', views.forecast, name='forecast'),
    url(r'^optdepth/$', views.opt_depth, name='optdepth'),
    url(r'^forecastscache/$', views.forecast_cache, name='forecastcache'),
    url(r'^cacheclear/$', views.cache_clear, name='cacheclear'),
    url(r'^optforecast/$', views.optimal_forecast, name='optforecast'),
    url(r'^cups/(?P<group_pk>[0-9]+)/$', views.cups, name='cups'),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^fe/$', views.forecast_events, name='forecastevents'),
]
