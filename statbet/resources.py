from django.db.models import ObjectDoesNotExist
from import_export import resources, fields
from .models import Event, MarketParameter, CupTeam, CupTotal


class EventResource(resources.ModelResource):
    score1 = fields.Field(column_name='score 1')
    score2 = fields.Field(column_name='score 2')
    win_val = fields.Field(column_name='win')
    x1 = fields.Field(column_name='1')
    x = fields.Field(column_name='X')
    x2  = fields.Field(column_name='2')

    # team1_formula1 = fields.Field(column_name='team home Ф1')
    # team2_formula1 = fields.Field(column_name='team away Ф1')
    # team1_formula2 = fields.Field(column_name='team home Ф2')
    # team2_formula2 = fields.Field(column_name='team away Ф2')
    #
    # teams_formula1 = fields.Field(column_name='team home/away Ф1')
    # teams_formula2 = fields.Field(column_name='team home/away Ф2')
    #
    # teams_f1_f2 = fields.Field(column_name='Ф1/Ф2')

    home_group = fields.Field(column_name='ГрД')
    away_group = fields.Field(column_name='ГрГ')

    mparam_x1 = MarketParameter.objects.get(id=5)
    mparam_x2 = MarketParameter.objects.get(id=8)
    mparam_x = MarketParameter.objects.get(id=7)

    class Meta:
        model = Event
        fields = (
            'date', 'country__name', 'cup__name', 'cup__season', 'cup__group__name', 'team1__name', 'team2__name',
            'score', 'win_val', 'score1', 'score2', 'x1', 'x', 'x2', 'event_id', 'th', 'ta', 'ratio', 'ratio_bid'
        )
        export_order = ('event_id', 'date', 'country__name', 'cup__name', 'cup__season', 'cup__group__name',
                        'team1__name', 'team2__name', 'score', 'win_val', 'score1', 'score2', 'x1', 'x', 'x2', 'th',
                        'ta',
                        # 'team1_formula1', 'team2_formula1',
                        # 'team1_formula2', 'team2_formula2'
                        )

    def dehydrate_home_group(self, e):
        try:
            ctg = CupTeam.objects.get(cup=e.cup, team=e.team1).cupteamgroup
        except ObjectDoesNotExist:
            return 0

        return ctg.group_home

    def dehydrate_away_group(self, e):
        try:
            ctg = CupTeam.objects.get(cup=e.cup, team=e.team2).cupteamgroup
        except ObjectDoesNotExist:
            return 0

        return ctg.group_away

    def dehydrate_score1(self, event):
        res = ''
        splited_score = event.score.split(':')
        if len(splited_score) == 2:
            return splited_score[0]
        return res

    def dehydrate_score2(self, event):
        res = ''
        splited_score = event.score.split(':')
        if len(splited_score) == 2:
            return splited_score[1]
        return res

    def dehydrate_win_val(self, event):
        return event.get_win()

    def dehydrate_x1(self, event):
        return event.get_bid(self.mparam_x1)

    def dehydrate_x2(self, event):
        return event.get_bid(self.mparam_x2)

    def dehydrate_x(self, event):
        return event.get_bid(self.mparam_x)


class CupTeamResource(resources.ModelResource):
    mparam_x1 = MarketParameter.objects.get(id=5)
    mparam_x2 = MarketParameter.objects.get(id=8)
    mparam_x = MarketParameter.objects.get(id=7)

    class Meta:
        model = Event
        fields = ('cup', 'team')
        export_order = ('cup', 'team', 'sad')


class CupTotalResource(resources.ModelResource):
    class Meta:
        model = CupTotal
        fields = (
            'cup__name', 'cup__season_start', 'where', 'ratio_of', 'group', 'ratio', 'value', 'matches'
        )
        export_order = ('cup__name', 'where', 'ratio_of')