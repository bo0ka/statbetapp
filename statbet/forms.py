from django import forms

from .models import Cup, CupGroup, ForecastCupCache


class ForecastForm(forms.Form):
    cup = forms.ModelChoiceField(queryset=None, label='Чемпионат')
    depth = forms.IntegerField(label='Глубина расчета для прошлых чемпионатов')
    w_matches = forms.IntegerField(label='W для более чем или равно матчей')
    just_total = forms.NullBooleanField(label='Только итоги')

    def __init__(self, *args, **kwargs):
        super(ForecastForm, self).__init__(*args, **kwargs)
        self.fields['cup'].queryset = Cup.objects.all()


class OptDepthForm(forms.Form):
    W_AVG = ForecastCupCache.W_AVG
    W_MAX = ForecastCupCache.W_MAX
    W_PIN = ForecastCupCache.W_BOOKMAKER
    W_CHOICES = (
        (W_AVG, 'средним'),
        (W_MAX, 'максимальным'),
        (W_PIN, 'Pinnacle'),
    )

    cupgroup = forms.ModelChoiceField(
        queryset=CupGroup.objects.all().order_by('country', 'name'),
        label='Группа чемпионатов',
        required=True,
        widget=forms.Select(
            attrs={
                "onChange": 'getCup()'
            }
        )
    )
    cup = forms.ModelChoiceField(
        queryset=Cup.objects.all(),
        label='Расчетный чемпионат',
        required=True
    )
    max_depth = forms.IntegerField(label='Максимальная глубина расчета')

    # calc_year = forms.DateField(label='Год начала расчетного сезона', required=True)
    calc_w = forms.ChoiceField(label='Расчитывать W по', choices=W_CHOICES, )
    use_cached = forms.BooleanField(required=False, label='Использовать кеш')

    def clean(self):
        cleaned_data = super(OptDepthForm, self).clean()
        cup = cleaned_data.get('cup')
        cup_group = cleaned_data.get('cupgroup')
        if not (cup and cup_group):
            raise forms.ValidationError('Заполните группу чемпионатов и чемпионат')
        if cup.group != cup_group:
            raise forms.ValidationError('Расчетный чемпионат должен быть из группы чемпионатов')


class ForecastCacheForm(forms.Form):
    pass


class OptForecastForm(forms.Form):
    W_AVG = ForecastCupCache.W_AVG
    W_MAX = ForecastCupCache.W_MAX
    W_PIN = ForecastCupCache.W_BOOKMAKER
    W_CHOICES = (
        (W_AVG, 'средним'),
        (W_MAX, 'максимальным'),
        (W_PIN, 'Pinnacle'),
    )
    calc_w = forms.ChoiceField(label='Результаты по', choices=W_CHOICES, )


class ForecastEventForm(forms.Form):
    start_date = forms.DateField()
    end_date = forms.DateField(required=False)
