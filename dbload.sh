#!/bin/zsh
# Использовать менеджмент команду dbload вместо этого sh
# dbload -i ./fixtures/results/slices/0:2000/ -v 3
# Параметр - путь к папке с файлами betexplorer.json и betexplorer_odds.json
# Например ./fixtures/results/slices/2000:france
if [ -n "$1" ] 
then
    echo "Начало обработки "
    echo "Загрузка "$1/betexplorer.json
    source ./.env/bin/activate
    python ./manage.py loadevents -i $1/betexplorer.json -v 3
    echo "Обновлеине событий"
    python ./manage.py eventsupdate -a cup-seasons -v 3
    echo "Обновлеине побед"
    python ./manage.py eventsupdate -a team-season-wins -v 3
    echo "Расчет результатов"
    python ./manage.py eventsupdate -a match-results
    python ./manage.py eventsupdate -a match-results-formula2
    echo "Обновлеине команд в чемпионатах"
    python ./manage.py eventsupdate -a cup-teams
    echo "Загрузка "$1/betexplorer_odds.json
    python ./manage.py loadbets -i $1/betexplorer_odds.json -v 3 -a avg
    python ./manage.py loadbets -i $1/betexplorer_odds.json -v 3 -a bookmaker
    deactivate
else
    echo "No dir specified"
fi